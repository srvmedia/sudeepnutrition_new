<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
      public function __construct()
      {
            parent::__construct();
            $this->load->model('admin_model');
            $this->load->library(array('form_validation','session'));
            $this->load->helper(array('url','html','form'));
            $this->user_id = $this->session->userdata('user_id');
            $this->user_name = $this->session->userdata('name');
      }


      public function index()
      {
            if(!empty($this->user_id)){
                  redirect(base_url('admin/dashboard'));
            }
            $data['title']="Login";
            $data['keyword']="";
            $data['description']="Login";
            
            $this->load->view('include/commonscript',$data);
            $this->load->view('include/header');
            $this->load->view('admin/login');
            $this->load->view('include/footer');
      }
      public function dashboard(){
            if(empty($this->user_id)){
                  redirect(base_url('admin'));
            }
            $this->load->model('consultant_model');
            $consultants = $this->consultant_model->get_all_consultants();
            
            $data['title']="Admin Dashboard";
            $data['keyword']="";
            $data['description']="Admin Dashboard";
            
            $this->load->view('include/commonscript', $data);
            $this->load->view('include/header');
            $this->load->view('admin/dashboard', array('name' => $this->user_name, 'consultants' => $consultants['consultants']));
            $this->load->view('include/footer');
      }
      public function post_login()
      {
            $commondata['title']="Login";
            $commondata['keyword']="login";
            $commondata['description']="Login";

            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_message('required', 'Enter %s');

            if ($this->form_validation->run() === FALSE)
            {  
                  $this->load->view('include/commonscript',$commondata);
                  $this->load->view('include/header');
                  $this->load->view('admin/login');
                  $this->load->view('include/footer');
            }
            else
            {   
                  $data = array(
                        'email' => $this->input->post('email'),
                        'password' => md5($this->input->post('password')),
                        );

                  $check = $this->admin_model->auth_check($data);

                  if($check != false)
                  {

                        $user = array(
                              'user_id' => $check->id,
                              'email' => $check->email,
                              'name' => $check->name
                        );

                        $this->session->set_userdata($user);

                        redirect( base_url('admin/dashboard') ); 
                  }

                  $res['error'] = "Invalid email or password!";

                  $this->load->view('include/commonscript',$commondata);
                  $this->load->view('include/header');
                  $this->load->view('admin/login', $res);
                  $this->load->view('include/footer');
            }

      }
      public function logout(){
            $this->session->sess_destroy();
            redirect(base_url('admin'));
      }  

      public function edit_consultant() {
            $response = array('status' => false);
            $this->load->model('consultant_model');
            if ($this->input->server('REQUEST_METHOD') == 'GET') {

                  $data['title']="Admin Edit";
                  $data['keyword']="";
                  $data['description']="Admin Edit";

                  if (empty($_GET['id'])) {
                        return $this->output
                              ->set_content_type('application/json')
                              ->set_status_header(200)
                              ->set_output(json_encode(array('error' => 'ID is required!')));   
                  }
                  
                  $id = $_GET['id'];
                  $this->load->model('common_model');
                  $consultant = $this->consultant_model->get_consultant_by_id($id);
                  // var_dump($consultant);
                  // die();
                  $states = $this->common_model->get_all_states();
                  $cities = $this->common_model->get_all_cities();
                  $job_types = $this->common_model->get_all_job_types();

                  $view_data = array(
                        'states' => $states,
                        'cities' => $cities,
                        'job_types' => $job_types,
                        'consultant' => $consultant
                  );

                  $res_arr['states'] = $states;
                  $res_arr['cities'] = $cities;
                  $res_arr['job_types'] = $job_types;
                  
                  $this->load->view('include/commonscript', $data);
                  $this->load->view('include/header');
                  $this->load->view('admin/edit_consultant', $view_data);
                  $this->load->view('include/footer');
                  return;
            }
            
            $data = $_POST;
            $response = $this->consultant_model->update_consultant($data);
            // return $this->output
            //             ->set_content_type('application/json')
            //             ->set_status_header(200)
            //             ->set_output(json_encode($response));
            redirect(base_url('admin/dashboard'));
      }

      public function accept_reject_registration() {
            $response = array('status' => false);
            $this->load->model('consultant_model');

            if (empty($_POST['id']) || empty($_POST['is_approved'])) {
                  return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(json_encode(array('error' => 'ID is required!')));   
            }
            
            if ($_POST['is_approved'] == 'true') {
                  $_POST['is_approved'] = true;
            } else {
                  $_POST['is_approved'] = false;
            }
            $data = $_POST;
            

            $response = $this->consultant_model->approve_reject_consultant($_POST['id'], $_POST['is_approved']);
            redirect(base_url('admin/dashboard'));
            // return $this->output
            //             ->set_content_type('application/json')
            //             ->set_status_header(200)
            //             ->set_output(json_encode($response));
      }
    
}
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
     public function __construct() { 
         parent::__construct(); 
         // $this->load->helper(array('form', 'url')); 
         $this->load->model('Frontmodel');
         $this->load->model('M_Welcome');
         $this->sub_page_name_to_id = array(
            "sobic"=>9,"critic"=>10,"malic"=>11,"sea"=>12,"fumaric"=>13,"Ammonium"=>14,
            "sodium"=>15,"calcium"=>16,"sulfate"=>17,"ferrous_sulfate"=>18,"manganese"=>19,"magnesium_sulfate"=>20,"zinc_sulfate"=>21,
            "potassium_iodide"=>22,"copper_gluconate"=>23,"magnesium_gluconate"=>24,"zinc_gluconate"=>25,"ferric_pyrophosphate"=>26,"ferric_orthophosphate"=>27,"magnesium_phosphate"=>28,
            "dipotassium_phosphate"=>29,"magnesium_oxide"=>30,"cupric_oxide"=>31,"zinc_oxide"=>32,"sodium_molybdate"=>33,"sodium_selenite"=>34,"chromium_picolinate"=>35,
            "calcium_citrate"=>36,"calcium_lactate"=>37,"poly_iron_complex"=>38,"potassium_chloride"=>39,"ferrous_fumarate"=>40,"ferrous_lactate"=>41,"reduced_iron"=>42,"magnesium_ascorbate"=>43,
            "selenomethionine"=>44,"chromium_chloride"=>45,"bakery"=>1,"savory"=>2,"dairy"=>3,"beverages"=>4,"meat"=>5,"confectionery"=>6,"infant"=>7,"dietary"=>8
        );
        $this->page_name_to_id = array(
            "product"=>3,
            "markets"=>1
        );
      }

    public function index() {
        $data['title']="Sudeep Nutrition";
        $data['keyword']="";
        $data['description']="Sudeep Nutrition Home page";
      
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('home' ,$data);
        $this->load->view('include/footer');
    }

    public function not_found() {
        $data['title']="Sudeep Nutrition";
        $data['keyword']="";
        $data['description']="Sudeep Nutrition Home page";
      
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('not_found' ,$data);
        $this->load->view('include/footer');
    }
    public function submitenquiry()
    {
    //  echo 'HI ENQUIRY I AM HRE TO SUBMIT';
    // die;
        date_default_timezone_set('asia/kolkata');
        $date=date('Y-m-d H:i:s');
        $data=array(
            
            'app_fname'=>htmlspecialchars($this->input->post('app_fname')),
            'app_email_address'=>htmlspecialchars($this->input->post('app_email_address')),
            'app_phone'=>htmlspecialchars($this->input->post('app_phone')),
            'app_city'=>htmlspecialchars($this->input->post('app_city')),
            'txtspeciality'=>htmlspecialchars($this->input->post('txtspeciality')),
            'app_course'=>htmlspecialchars($this->input->post('app_course')),
            'utm_source'=>htmlspecialchars($this->input->post('utm_source')),
            'utm_medium'=>htmlspecialchars($this->input->post('utm_medium')),
            'utm_campaign'=>htmlspecialchars($this->input->post('utm_campaign')),
            'date'=>$date
        );

        $query=$this->M_Welcome->insertenquiry($data);

            if($query=='1')
            {
                header("Location: thankyou");
            }
            else
            {
                header("Location: thankyou");
            }
    }

    public function what_services($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('what_services_provide' ,$data);
        $this->load->view('include/footer');
    }

    public function contract($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('contract' ,$data);
        $this->load->view('include/footer');
    }

    function contact_us(){
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('contact_us' ,$data);
        $this->load->view('include/footer');
    }

    public function product_information($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('product_information' ,$data);
        $this->load->view('include/footer');
    }

    public function compliance($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('compliance' ,$data);
        $this->load->view('include/footer');
    }

    public function certification($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('certification' ,$data);
        $this->load->view('include/footer');
    }

    public function join_team($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('join_our_team' ,$data);
        $this->load->view('include/footer');
    }

    public function what_we_do($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('what_we_do' ,$data);
        $this->load->view('include/footer');
    }

    public function sample($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('request_sample' ,$data);
        $this->load->view('include/footer');
    }
    
    public function about_us($data_id = 0) {
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('about_us' ,$data);
        $this->load->view('include/footer');
    }

    public function market() {
        $sub_tab_id = $this->uri->segment(2);
        $tab_id = $this->uri->segment(3);
        $sub_tab_id=$this->sub_page_name_to_id[$sub_tab_id];
        $tab_id=$this->page_name_to_id[$tab_id];
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $data['main_header']=$this->Frontmodel->get_main_header($sub_tab_id);
        $data['content']=$this->Frontmodel->get_market_content($tab_id,$sub_tab_id);
        $data['challenges']=$this->Frontmodel->get_challenges_market($tab_id,$sub_tab_id);
        $data['products']=$this->Frontmodel->get_product_list_market($tab_id,$sub_tab_id);
        $data['sub_tab']=$sub_tab_id;
        if($data['main_header']== NULL || $data['content']== NULL){
            redirect('welcome/not_found');
        }
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('market1' ,$data);
        $this->load->view('include/footer');
    }
    
    
    
    public function products($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $sub_tab_id = $this->uri->segment(2);
        $tab_id = $this->uri->segment(3);
        $sub_tab_id=$this->sub_page_name_to_id[$sub_tab_id];
        $tab_id=$this->page_name_to_id[$tab_id];
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $data['main_header']=$this->Frontmodel->get_main_header($sub_tab_id);
        $data['product_content']=$this->Frontmodel->get_product_main_content($tab_id,$sub_tab_id);
        $data['applications']=$this->Frontmodel->get_product_application($tab_id,$sub_tab_id);
        $data['advantages']=$this->Frontmodel->get_product_advantages($tab_id,$sub_tab_id);
        $data['product_sub_section']=$this->Frontmodel->get_product_sub_section($tab_id,$sub_tab_id);
        $data['product_sub_section_icons']=$this->Frontmodel->get_product_sub_section_icon($tab_id,$sub_tab_id);
        $data['sub_tab']=$sub_tab_id;
        if($data['main_header']== NULL || $data['product_content']== NULL){
            redirect('welcome/not_found');
        }
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('encapsulate_sorbic_acid1' ,$data);
        $this->load->view('include/footer');
    } 

    public function technology($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('technology_encapsulation' ,$data);
        $this->load->view('include/footer');
    }
    
    public function granulation($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('granulation' ,$data);
        $this->load->view('include/footer');
    }

    public function hotmelt($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('hotmelt' ,$data);
        $this->load->view('include/footer');
    }

    public function spray($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('spray' ,$data);
        $this->load->view('include/footer');
    }

    public function blending($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('blending' ,$data);
        $this->load->view('include/footer');
    }

    public function microencapsulation($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('microencapsulation' ,$data);
        $this->load->view('include/footer');
    }

    public function product_list($data_id = 0) {
        $program_id = $this->input->get('program_id');
        $data_id = $this->uri->segment(3);
        $data['title']='Sudeep nutrition';
        $data['keyword']="";
        $data['description']="Sudeep nutrition";
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('product_list' ,$data);
        $this->load->view('include/footer');
    }
}
// echo "<pre>";print_r($main_header[0]->sub_section_header);die();
?>
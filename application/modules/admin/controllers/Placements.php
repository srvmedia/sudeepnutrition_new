<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Placements extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['placements']=$this->commonmodel->select(NULL,'tbl_placements');
		$result['page']="listplacements";
		$result['mainpage']="home";
		$this->template->load('template', 'listplacements',$result);
		
	}
	public function add()
	{
		
		$result['page']="addplacements";
		$result['mainpage']="home";
		$this->template->load('template', 'addplacements',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'placements_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_placements',$condition);
			   redirect(base_url().'admin/placements');
	}
	public function insert()
	{
		if($this->input->post('placementsid')!=NULL)
		{
			$id=$this->input->post('placementsid');
			$description=$this->input->post('description');
			$candidatename=$this->input->post('candidatename');
			$companyname=$this->input->post('companyname');
			$intdate=$this->input->post('intdate');
			$imagename= $_FILES['image']['name'];
			
			if($imagename!="")
			{
				$file_name = uniqid().$_FILES['image']['name'];
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."placementsprofile/".$file_name;
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					      'placements_candidatename'  => $candidatename,
					    'placements_company'  => $companyname,
						 'placements_date'  => $intdate,
						 'placements_description'  => $description,
					   'placements_candidateimage'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					    'placements_candidatename'  => $candidatename,
					    'placements_company'  => $companyname,
						 'placements_date'  => $intdate,
						 'placements_description'  => $description
				   );

			}
			$condition = array(
                   'placements_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_placements',$condition,$newdata);
		}
		else
		{
		
			$id=$this->input->post('placementsid');
			$description=$this->input->post('description');
			$candidatename=$this->input->post('candidatename');
			$companyname=$this->input->post('companyname');
			$intdate=$this->input->post('intdate');
			$file_name = uniqid().$_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."placementsprofile/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					    'placements_candidatename'  => $candidatename,
					    'placements_company'  => $companyname,
						 'placements_date'  => $intdate,
						 'placements_description'  => $description,
					   'placements_candidateimage'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_placements');
		}
	
		redirect(base_url().'admin/placements');
		
	}
	
	
	public function edit()
	{
		$result['page']="placements";
		$result['mainpage']="home";
		$id=$this->uri->segment('4');
		$condition = array(
                   'placements_id'  => $id
               );
		$result['placements']=$this->commonmodel->select($condition,'tbl_placements');
		$this->template->load('template', 'addplacements',$result);
	}
	
}

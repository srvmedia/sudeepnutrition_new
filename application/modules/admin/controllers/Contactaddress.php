<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactaddress extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['contactaddress']=$this->commonmodel->select(NULL,'tbl_contactaddress');
		$result['page']="listcontactaddress";
		$this->template->load('template', 'listcontactaddress',$result);
		
	}
	
	public function delete($id)
	{
		$condition = array(
                   'contactaddress_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_contactaddress',$condition);
			   redirect(base_url().'admin/contactaddress');
	}
	public function insert()
	{
		if($this->input->post('contactaddressid')!=NULL)
		{
			$id=$this->input->post('contactaddressid');
			$corporateaddress=$this->input->post('corporateaddress');
			$campusaddress=$this->input->post('campusaddress');
			$phone=$this->input->post('phone');
			$email=$this->input->post('email');
				$newdata = array(
					   'contactaddress_address'  => $corporateaddress,
					   'contactaddress_campusaddress'  => $campusaddress,
					   'contactaddress_phone'  => $phone,
					   'contactaddress_email'  => $email
				   );
			$condition = array(
                   'contactaddress_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_contactaddress',$condition,$newdata);
		}
		else
		{
		
			$corporateaddress=$this->input->post('corporateaddress');
			$campusaddress=$this->input->post('campusaddress');
			$phone=$this->input->post('phone');
			$email=$this->input->post('email');
			$newdata = array(
					   'contactaddress_address'  => $corporateaddress,
					   'contactaddress_campusaddress'  => $campusaddress,
					   'contactaddress_phone'  => $phone,
					   'contactaddress_email'  => $email
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_contactaddress');
		}
	
		redirect(base_url().'admin/contactaddress');
		
	}
	
	
	public function edit()
	{
		$result['page']="contactaddress";
		$id=$this->uri->segment('4');
		$condition = array(
                   'contactaddress_id'  => $id
               );
		$result['contactaddress']=$this->commonmodel->select($condition,'tbl_contactaddress');
		$this->template->load('template', 'addcontactaddress',$result);
	}
	
}

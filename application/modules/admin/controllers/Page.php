<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$join=array("tbl_page as parent"=>"tbl_page.page_parent=parent.page_id");
		$fields="tbl_page.*,parent.page_name as parent";
		$result['pagelist']=$this->commonmodel->select_joinfieled(NULL,'tbl_page',$join,$fields);
		$result['page']="listpage";
		$this->template->load('template', 'listpage',$result);
		
	}
	public function add()
	{
		
		$result['page']="addpage";
		$result['pagetab']=array();
		$result['allpages']=$this->commonmodel->select(NULL,'tbl_page');
		$this->template->load('template', 'addpage',$result);
		
	}
	
	public function delete($id)
	{
		$condition = array(
                   'page_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_page',$condition);
			   redirect(base_url().'admin/page');
	}
	public function insert()
	{
		if($this->input->post('pageid')!=NULL)
		{
			$tabtitle=$this->input->post('tabtitle');
			$tabcontent=$this->input->post('tabcontent');
			
			$id=$this->input->post('pageid');
			$content=$this->input->post('content');
			$pname=$this->input->post('pname');
			$parent=$this->input->post('parent');
			$asmenu=$this->input->post('asmenu');
			$hassidemenu=$this->input->post('hassidemenu');
		
			$file_name = uniqid().$_FILES['image']['name'];
			
			if($_FILES['image']['name']!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."homeslider/".$file_name;
				move_uploaded_file($file_tmp,$filePath);
				$newdata = array(
					   'page_name'  => $pname,
					    'page_parent'  => $parent,
						'page_showinmenu'  => $asmenu,
						'page_hassidemenu	'  => $hassidemenu,
					    'page_content'  => $content,
						'page_banner'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   'page_name'  => $pname,
					    'page_parent'  => $parent,
						'page_showinmenu'  => $asmenu,
						'page_hassidemenu	'  => $hassidemenu,
					    'page_content'  => $content,
				   );
			}
			$condition = array(
                   'page_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_page',$condition,$newdata);
			
			$tabcount=count($tabtitle);
			
			$condition = array(
                   'pagetab_pageid'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_pagetab',$condition);
			 for($i=0;$i<$tabcount;$i++)
			 {
				 $tatitle=$tabtitle[$i];
				 $tacontent=$tabcontent[$i];
				 
				 $newdata1 = array(
					   'pagetab_pageid'  => $id, 
					   'pagetab_title'  => $tatitle,
					   'pagetab_content'  => $tacontent
				   );
				   if($tatitle!="")
				   {
				   		$this->commonmodel->insert_entry($newdata1,'tbl_pagetab');
				   }
			 }
		}
		else
		{
			$tabtitle=$this->input->post('tabtitle');
			$tabcontent=$this->input->post('tabcontent');
			
			$content=$this->input->post('content');
			$pname=$this->input->post('pname');
			$parent=$this->input->post('parent');
			$asmenu=$this->input->post('asmenu');
			$hassidemenu=$this->input->post('hassidemenu');
			
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."homeslider/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'page_name'  => $pname, 
					   'page_parent'  => $parent,
					   'page_showinmenu'  => $asmenu,
					   'page_hassidemenu	'  => $hassidemenu,
					    'page_content'  => $content,
						'page_banner'  => $file_name
				   );
			 $id=$this->commonmodel->insert_entry($newdata,'tbl_page');
			 $tabcount=count($tabtitle);
			 for($i=0;$i<$tabcount;$i++)
			 {
				 $tatitle=$tabtitle[$i];
				 $tacontent=$tabcontent[$i];
				 
				 $newdata1 = array(
					   'pagetab_pageid'  => $id, 
					   'pagetab_title'  => $tatitle,
					   'pagetab_content'  => $tacontent
				   );
				   if($tatitle!="")
				   {
				   		$this->commonmodel->insert_entry($newdata1,'tbl_pagetab');
				   }
			 }
			 
		}
	
		redirect(base_url().'admin/page');
		
	}
	
	
	public function edit()
	{
		$result['page']="page";
		$id=$this->uri->segment('4');
		$result['allpages']=$this->commonmodel->select(NULL,'tbl_page');
		
		$condition = array(
                   'pagetab_pageid'  => $id
               );
		$result['pagetab']=$this->commonmodel->select($condition,'tbl_pagetab');
		
		$condition = array(
                   'page_id'  => $id
               );
		$result['pagelist']=$this->commonmodel->select($condition,'tbl_page');
		$this->template->load('template', 'addpage',$result);
	}
	
}

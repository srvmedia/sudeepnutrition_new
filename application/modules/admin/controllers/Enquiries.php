<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Enquiries extends CI_Controller {
	
	
	
	
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	public function __construct()
	{
		
		
		parent::__construct();
		
		
		if($this->session->userdata('username') == FALSE)
		{
			
			
			redirect('admin/login');
			
			
		}
		
		
		
		//p		rint_r($result['adminuser']);
		
		
		// 		Your own constructor code
	}
	
	
	public function index()
	{
		
		
		
		$result['enquiries']=$this->commonmodel->select_dec_asc(NULL,'tbl_enquiries','enquiries_date');
		
		
		$result['page']="listenquiries";
		
		
		$this->template->load('template', 'listenquiries',$result);
		
		
		
	}
	public function scholarships()
	{
		
		
		
		$result['enquiries']=$this->commonmodel->select_dec_asc(NULL,'tbl_scholarship','date_of_apply');
		
		
		$result['page']="listscholarships";
		
		
		$this->template->load('template', 'listscholarships',$result);
		
		
		
	}
	
	
	
	
	public function delete($id)
	{
		
		
		$condition = array(
		'enquiries_id'  => $id
		);
		
		
		$this->commonmodel->delete_entry('tbl_enquiries',$condition);
		
		
		redirect(base_url().'admin/enquiries');
		
		
	}
	
	
	function export_to_excel()
	{
		
		$setData = "";
		
		$setCounter = 0;
		
		$setExcelName = "enquiry";
		
		$setMainHeader="";
		
		$enquiry=$this->commonmodel->select_dec_asc(NULL,'tbl_enquiries','enquiries_date');
		
		
		$setMainHeader .="No"."\t ";
		
		
		$setMainHeader .="Category"."\t ";
		
		
		$setMainHeader .="Subject"."\t ";
		
		
		$setMainHeader .="Name"."\t ";
		
		
		$setMainHeader .="Email"."\t ";
		
		
		$setMainHeader .="Phone"."\t ";
		
		
		$setMainHeader .="Date"."\t ";
		
		
		$setMainHeader .="Message"."\t ";
		
		
		$i=0;
		
		
		foreach($enquiry as $value)  {
			
			
			$i++;
			
			
			$rowLine = $i. "\t ";
			
			
				
				
				$rowLine .=$this->setcleanval($value['enquiries_category']).$this->setcleanval($value['enquiries_suject']).$this->setcleanval($value['enquiries_name']).
				$this->setcleanval($value['enquiries_email']).$this->setcleanval($value['enquiries_phone']).$this->setcleanval($value['enquiries_date']).$this->setcleanval($value['enquiries_message']);
				
		
			
		
			$setData .= trim($rowLine)."\n";
			
			
		}

		
		// $setData = str_replace("\r", "", $setData);
		
		
		
		if ($setData == "") {
			
			
			$setData = "\nno matching records found\n";
			
			
		}
		
		
		
		
		
		
		
		//T		his Header is used to make data download instead of display the data
		//header("Content-Type: application/vnd.ms-excel");
		
		header("Content-Type: application/xls"); 
		
		header("Content-Disposition: attachment; filename=".$setExcelName."_Reoprt.xls");
		
		
		
		header("Pragma: no-cache");
		
		
		header("Expires: 0");
		
		
		
		//I		t will print all the Table row as Excel file row with selected column name as header.
		echo ucwords($setMainHeader)."\n".$setData."\n";
		
		
	}
	
		function setcleanval($value)
  {
	  $value=trim($value);
 $value = preg_replace("/\t/", "  ", $value);
	    if(!isset($value) || $value == "")  {
      $value = "\t ";
	  
    }   else  {
//It escape all the special charactor, quotes from the data.
      $value = strip_tags(str_replace('"', '', $value));
      $value =   '"'.$value .'"'. "\t ";
    }
	return $value;
  }

	
	
	
	
}



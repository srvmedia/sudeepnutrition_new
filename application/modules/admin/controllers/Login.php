<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			$this->load->model('loginmodel');
            // Your own constructor code
       }
	public function index()
	{
		
		$this->load->view('login');
	}
	public function logout()
	{
		
		$this->session->unset_userdata('username');
		redirect(base_url().'admin/login');
	}
	public function dashboard()
	{
		$username= $this->session->userdata('username');
		
		$this->load->view('sidebar');
	}
	public function authenticate()
	{
		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}
		//$condtionarray=array('ip'=>$ip );
		//$result1=$this->loginmodel->select($condtionarray,'tbl_whitelist');
		//if(!$result1)
		//{

         //$os=$_SERVER['HTTP_USER_AGENT'];
         //$this->session->set_flashdata('message', 'We consider you as spammer');
         //$data=array(
         //   	'ip'=>$ip,
         //   	'os'=>$os,
         //   	'access_time'=>date('Y-m-d H:i:s'));
         //$result=$this->loginmodel->insert_entry($data,'tbl_blacklist');
		//	redirect(base_url().'admin/login');
		//}
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$condtionarray=array('admin_username'=>$username,'admin_password'=>md5($password) );
		$result=$this->loginmodel->select($condtionarray,'tbl_admin');
		if($result)
		{
			$newdata = array(
                   'username'  => $username,
               );

			$this->session->set_userdata($newdata);
			redirect(base_url().'admin/dashboard');
		}
		else
		{
			$this->session->set_flashdata('message', 'Invalid Login Details');
			redirect(base_url().'admin/login');
		}
	}
}

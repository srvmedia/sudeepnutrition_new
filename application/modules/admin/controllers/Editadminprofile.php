<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editadminprofile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
            // Your own constructor code
       }
	public function index()
	{
		
		$result['page']="editprofile";
		$id="1";
		$condition = array(
                   'admin_id'  => $id
               );
			   
		$result['details']=$this->commonmodel->select($condition,'tbl_admin');

		$this->template->load('template', 'editprofile',$result);
	}
	public function update()
	{
			$id="1";
			$dname=$this->input->post('dname');
			$password=$this->input->post('password');
			
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				$filePath=$_SERVER["DOCUMENT_ROOT"]."/OTHSRRWEDG/cms/assets/adminprofile/".$file_name;
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
                   'admin_dname'  => $dname,
				   'admin_image'  => $file_name
               );
			}
			else
			{
				$newdata = array(
                   'admin_dname'  => $dname,
               );
			}
			$condition = array(
                   'admin_id'  => $id
               );
			   if($password!="")
			   {
				   $newdata['admin_password']=$password;
			   }
			$this->commonmodel->update_entry('tbl_admin',$condition,$newdata);
			redirect(base_url().'admin/editadminprofile');
			
		}
		

}

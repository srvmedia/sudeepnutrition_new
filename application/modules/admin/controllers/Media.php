<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['medias']=$this->commonmodel->select(NULL,'tbl_media');
		$result['page']="listmedia";
		$this->template->load('template', 'listmedia',$result);
		
	}
	public function add()
	{
		
		$result['page']="addmedia";
		$this->template->load('template', 'addmedia',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'media_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_media',$condition);
			   redirect(base_url().'admin/media');
	}
	public function insert()
	{
		if($this->input->post('mediaid')!=NULL)
		{
			$id=$this->input->post('mediaid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."media/".$file_name;
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   'media_description'  => $description,
					   'media_title'  => $title,
					   'media_image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   'media_description'  => $description,
					   'media_title'  => $title,
				   );

			}
			$condition = array(
                   'media_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_media',$condition,$newdata);
		}
		else
		{
		
			$id=$this->input->post('mediaid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."media/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'media_description'  => $description,
					   'media_title'  => $title,
					   'media_image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_media');
		}
	
		redirect(base_url().'admin/media');
		
	}
	
	
	public function edit()
	{
		$result['page']="media";
		$id=$this->uri->segment('4');
		$condition = array(
                   'media_id'  => $id
               );
		$result['media']=$this->commonmodel->select($condition,'tbl_media');
		$this->template->load('template', 'addmedia',$result);
	}
	
}

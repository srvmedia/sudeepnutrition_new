<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flashmessage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['flash']=$this->commonmodel->select(NULL,'tbl_homeflashmessage');
		$result['page']="listflashmessage";
		$result['mainpage']="home";
		$this->template->load('template', 'listflashmessage',$result);
		
	}
	public function add()
	{
		
		$result['page']="addflashmessage";
		$result['mainpage']="home";
		$this->template->load('template', 'addflashmessage',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'homeflashmessage_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_homeflashmessage',$condition);
			   redirect(base_url().'admin/flashmessage');
	}
	public function insert()
	{
		if($this->input->post('flashid')!=NULL)
		{
			$id=$this->input->post('flashid');
			$message=$this->input->post('message');
			$messageurl=$this->input->post('messageurl');
		
				
				$newdata = array(
					   'homeflashmessage_content'  => $message,
					   'homeflashmessage_url'  => $messageurl
					
				   );

			$condition = array(
                   'homeflashmessage_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_homeflashmessage',$condition,$newdata);
		}
		else
		{
	
			$message=$this->input->post('message');
			$messageurl=$this->input->post('messageurl');
		
			$newdata = array(
					   'homeflashmessage_content'  => $message,
					    'homeflashmessage_url'  => $messageurl
					
				   );

			 $this->commonmodel->insert_entry($newdata,'tbl_homeflashmessage');
		}
	
		redirect(base_url().'admin/flashmessage');
		
	}
	
	
	public function edit()
	{
		$result['page']="flash";
		$result['mainpage']="home";
		$id=$this->uri->segment('4');
		$condition = array(
                   'homeflashmessage_id'  => $id
               );
		$result['flash']=$this->commonmodel->select($condition,'tbl_homeflashmessage');
		$this->template->load('template', 'addflashmessage',$result);
	}
	
}

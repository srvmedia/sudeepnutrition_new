<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		// $result['gallerys']=$this->commonmodel->select(NULL,'tbl_gallery');
		// $result['page']="listgallery";
		// $this->template->load('template', 'listgallery',$result);
		$result['imagelist']=$this->commonmodel->select(NULL,'tbl_gallerynew');
		//print_r($result['imagelist']);die();
		$result['page']="addgallerynew";
		$this->template->load('template', 'addgallerynew',$result);
		
	}
	public function add()
	{
		
		$result['page']="addgallery";
		$this->template->load('template', 'addgallery',$result);
		
	}

		public function addnew()
	{
		$result['imagelist']=$this->commonmodel->select(NULL,'tbl_gallerynew');
		//print_r($result['imagelist']);die();
		$result['page']="addgallerynew";
		$this->template->load('template', 'addgallerynew',$result);
		
	}

	public function image_gallery($id)
	{
            $condition = array(
                   'gallery_id'  => $id
               );
		$result['page']="fullgallery";
		//$id=$this->uri->segment('4');
        $result['gallery_id']=$id;
       // print_r($result['gallery_id']);die();
		$result['imagelist']=$this->commonmodel->select($condition,'tbl_galleryfull');
	
		//$result['news_id']=$_GET['id'];
		$this->template->load('template', 'fullgallery',$result);
	}

	public function delete($id)
	{
		$condition = array(
                   'gallery_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_gallery',$condition);
			   redirect(base_url().'admin/gallery');
	}

	public function deletenew($id)
	{
		$condition = array(
                   'gallery_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_gallerynew',$condition);
			   $this->commonmodel->delete_entry('tbl_galleryfull',$condition);
			   redirect(base_url().'admin/gallery/addnew');
	}

	public function deletegallery($id)
	{
		$condition = array(
                   'galleryfull_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_galleryfull',$condition);
			   redirect(base_url().'admin/gallery/addnew');
	}

	public function insert()
	{
		if($this->input->post('galleryid')!=NULL)
		{
			$id=$this->input->post('galleryid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."gallery/".$file_name;
				//echo UPLOAD_URL;
					
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   'gallery_description'  => $description,
					   'gallery_title'  => $title,
					   'gallery_image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   'gallery_description'  => $description,
					   'gallery_title'  => $title,
				   );

			}
			$condition = array(
                   'gallery_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_gallery',$condition,$newdata);
		}
		else
		{
		
			$id=$this->input->post('galleryid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			// $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."gallery/".$file_name;
			//echo UPLOAD_URL;
 			$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'gallery_description'  => $description,
					   'gallery_title'  => $title,
					   'gallery_image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_gallery');
		}
	
		redirect(base_url().'admin/gallery');
		
	}


	public function insertnew()
	{
		if($this->input->post('galleryid')!=NULL)
		{
			$id=$this->input->post('galleryid');
			
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."gallery/".$file_name;
				//echo UPLOAD_URL;
					
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   
					   'title'  => $title,
					   'image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   
					   'title'  => $title,
				   );

			}
			$condition = array(
                   'gallery_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_gallerynew',$condition,$newdata);
		}
		else
		{
		
			
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			// $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."gallery/".$file_name;
			//echo UPLOAD_URL;
 			$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   
					   'title'  => $title,
					   'image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_gallerynew');
		}
	
		redirect(base_url().'admin/gallery/addnew');
		
	}

	public function insert_galleryfull_old()
	{
		if($this->input->post('galleryid')!=NULL)
		{
			$id=$this->input->post('galleryid');
			
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."gallery/".$file_name;
				//echo UPLOAD_URL;
					
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   
					   'title'  => $title,
					   'image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   
					   'title'  => $title,
				   );

			}
			$condition = array(
                   'galleryfull_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_galleryfull',$condition,$newdata);
		}
		else
		{
		
			$gallery_id=$this->input->post('gallery_id');
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			// $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."gallery/".$file_name;
			//echo UPLOAD_URL;
 			$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'gallery_id'=> $gallery_id,
					   'title'  => $title,
					   'image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_galleryfull');
		}
	
		redirect(base_url().'admin/gallery/addnew');
		
	}

	public function insert_galleryfull()
	{
		if($this->input->post('galleryid')!=NULL)
		{
			$id=$this->input->post('galleryid');
			
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp ="gallery_".$gallery_id."_".$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."gallery/".$file_name;
				//echo UPLOAD_URL;
					
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   
					   'title'  => $title,
					   'image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   
					   'title'  => $title,
				   );

			}
			$condition = array(
                   'galleryfull_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_galleryfull',$condition,$newdata);
		}
		else
		{
		
			$gallery_id=$this->input->post('gallery_id');
			$title=$this->input->post('title');


            $cpt=0;
  $cpt = count($_FILES['image']['name']);
  //echo $cpt;
 
   for($i=0; $i<$cpt; $i++){

   
		$file_name ="gallery_".$gallery_id."_".$_FILES['image']['name'][$i];
		$file_tmp =$_FILES['image']['tmp_name'][$i];
		
		// 	  //home/content/17/8657417/html/test/application/modules/admin/controllers/Events.php
	
			$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;

			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'gallery_id'=> $gallery_id,
					   'title'  => $title,
					   'image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_galleryfull');
		}










			// $file_name = $_FILES['image']['name'];
			// $file_tmp =$_FILES['image']['tmp_name'];
			
 		// 	$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/gallery/".$file_name;
			// move_uploaded_file($file_tmp,$filePath);
			// $newdata = array(
			// 		   'gallery_id'=> $gallery_id,
			// 		   'title'  => $title,
			// 		   'image'  => $file_name
			// 	   );
			//  $this->commonmodel->insert_entry($newdata,'tbl_galleryfull');
		}
	
		redirect(base_url().'admin/gallery/addnew');
		
	}
	
	
	public function edit()
	{
		$result['page']="gallery";
		$id=$this->uri->segment('4');
		$condition = array(
                   'gallery_id'  => $id
               );
		$result['gallery']=$this->commonmodel->select($condition,'tbl_gallery');
		$this->template->load('template', 'addgallery',$result);
	}

		public function editnew()
	{
		$result['page']="addgallerynew";
		$result['imagelist']=$this->commonmodel->select(NULL,'tbl_gallerynew');
		$id=$this->uri->segment('4');
		$condition = array(
                   'gallery_id'  => $id
               );
		$result['gallery']=$this->commonmodel->select($condition,'tbl_gallerynew');
		$this->template->load('template', 'addgallerynew',$result);
	}

	public function editgallery()
	{
		$result['page']="fullgallery";
		$result['imagelist']=$this->commonmodel->select(NULL,'tbl_galleryfull');
		$id=$this->uri->segment('4');
		$condition = array(
                   'galleryfull_id'  => $id
               );
		$result['gallery']=$this->commonmodel->select($condition,'tbl_galleryfull');
		$this->template->load('template', 'fullgallery',$result);
	}
	
}

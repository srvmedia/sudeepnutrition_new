<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homenews extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['homenews']=$this->commonmodel->select(NULL,'tbl_homenewsevents');
		$result['page']="listhomenews";
		$result['mainpage']="home";
		$this->template->load('template', 'listhomenews',$result);
		
	}
	
	public function delete($id)
	{
		$condition = array(
                   'homenewsevents_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_homenewsevents',$condition);
			   redirect(base_url().'admin/homenews');
	}
	public function insert()
	{
		if($this->input->post('homenewseventsid')!=NULL)
		{
			$id=$this->input->post('homenewseventsid');
			$description=$this->input->post('description');
				$newdata = array(
						 'homenewsevents_content'  => $description
				   );

			
			$condition = array(
                   'homenewsevents_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_homenewsevents',$condition,$newdata);
		}
		
	
		redirect(base_url().'admin/homenews');
		
	}
	
	
	public function edit()
	{
		$result['page']="homenews";
		$result['mainpage']="home";
		$id=$this->uri->segment('4');
		$condition = array(
                   'homenewsevents_id'  => $id
               );
		$result['homenews']=$this->commonmodel->select($condition,'tbl_homenewsevents');
		$this->template->load('template', 'addhomenews',$result);
	}
	
}

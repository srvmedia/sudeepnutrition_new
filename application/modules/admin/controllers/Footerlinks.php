<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Footerlinks extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['content']=$this->commonmodel->select(NULL,'tbl_footerlinks');
		$result['page']="listfooterlinks";
		$this->template->load('template', 'listfooterlinks',$result);
		
	}
	
	
	public function insert()
	{
	
			$link_1=$this->input->post('link_1');
			$link_2=$this->input->post('link_2');
			$link_3=$this->input->post('link_3');
			$link_4=$this->input->post('link_4');
			$link_5=$this->input->post('link_5');
			$link_6=$this->input->post('link_6');
			$link_7=$this->input->post('link_7');
			$link_8=$this->input->post('link_8');
			$link_9=$this->input->post('link_9');
			$link_10=$this->input->post('link_10');
			$link_11=$this->input->post('link_11');
			$link_12=$this->input->post('link_12');
			$link_13=$this->input->post('link_13');
			$link_14=$this->input->post('link_14');
			$link_15=$this->input->post('link_15');
			$link_16=$this->input->post('link_16');
			$link_17=$this->input->post('link_17');
			$link_18=$this->input->post('link_18');
			$link_19=$this->input->post('link_19');
			$link_20=$this->input->post('link_20');
			$link_21=$this->input->post('link_21');
			$link_22=$this->input->post('link_22');
			$link_23=$this->input->post('link_23');
			$link_24=$this->input->post('link_24');
			$link_25=$this->input->post('link_25');
			$link_26=$this->input->post('link_26');
			$link_27=$this->input->post('link_27');
			$link_28=$this->input->post('link_28');
			$link_29=$this->input->post('link_29');
			$link_30=$this->input->post('link_30');
			$link_31=$this->input->post('link_31');
			$link_32=$this->input->post('link_32');
			$link_33=$this->input->post('link_33');
			$link_34=$this->input->post('link_34');
			$link_35=$this->input->post('link_35');
			$link_36=$this->input->post('link_36');
			$link_37=$this->input->post('link_37');
			$link_38=$this->input->post('link_38');
			$text_1=$this->input->post('text_1');
			$text_2=$this->input->post('text_2');
			$text_3=$this->input->post('text_3');
			$text_4=$this->input->post('text_4');
			$text_5=$this->input->post('text_5');
			$text_6=$this->input->post('text_6');
			$text_7=$this->input->post('text_7');
			$text_8=$this->input->post('text_8');
			$text_9=$this->input->post('text_9');
			$text_10=$this->input->post('text_10');
			$text_11=$this->input->post('text_11');
			$text_12=$this->input->post('text_12');
			$text_13=$this->input->post('text_13');
			$text_14=$this->input->post('text_14');
			$text_15=$this->input->post('text_15');
			$text_16=$this->input->post('text_16');
			$text_17=$this->input->post('text_17');
			$text_18=$this->input->post('text_18');
			$text_19=$this->input->post('text_19');
			$text_20=$this->input->post('text_20');
			$text_21=$this->input->post('text_21');
			$text_22=$this->input->post('text_22');
			$text_23=$this->input->post('text_23');
			$text_24=$this->input->post('text_24');
			$text_25=$this->input->post('text_25');
			$text_26=$this->input->post('text_26');
			$text_27=$this->input->post('text_27');
			$text_28=$this->input->post('text_28');
			
			$text_36=$this->input->post('text_36');
			$text_37=$this->input->post('text_37');
			$text_38=$this->input->post('text_38');
			$id=1;
					$newdata = array(
					   'link_1'  => $link_1,
					   'link_2'  => $link_2,
					   'link_3'  => $link_3,
					   'link_4'  => $link_4,
					   'link_5'  => $link_5,
					   'link_6'  => $link_6,
					   'link_7'  => $link_7,
					   'link_8'  => $link_8,
					   'link_9'  => $link_9,
					   'link_10'  => $link_10,
					   'link_11'  => $link_11,
					   'link_12'  => $link_12,
					   'link_13'  => $link_13,
					   'link_14'  => $link_14,
					   'link_15'  => $link_15,
					   'link_16'  => $link_16,
					   'link_17'  => $link_17,
					   'link_18'  => $link_18,
					   'link_19'  => $link_19,
					   'link_20'  => $link_20,
					   'link_21'  => $link_21,
					   'link_22'  => $link_22,
					   'link_23'  => $link_23,
					   'link_24'  => $link_24,
					   'link_25'  => $link_25,
					   'link_26'  => $link_26,
					   'link_27'  => $link_27,
					   'link_28'  => $link_28,
					   'link_29'  => $link_29,
					   'link_30'  => $link_30,
					   'link_31'  => $link_31,
					   'link_32'  => $link_32,
					   'link_33'  => $link_33,
					   'link_34'  => $link_34,
					   'link_35'  => $link_35,
					   'link_36'  => $link_36,
					   'link_37'  => $link_37,
					   'link_38'  => $link_38,
					   'text_1'  => $text_1,
					   'text_2'  => $text_2,
					   'text_3'  => $text_3,
					   'text_4'  => $text_4,
					   'text_5'  => $text_5,
					   'text_6'  => $text_6,
					   'text_7'  => $text_7,
					   'text_8'  => $text_8,
					   'text_9'  => $text_9,
					   'text_10'  => $text_10,
					   'text_11'  => $text_11,
					   'text_12'  => $text_12,
					   'text_13'  => $text_13,
					   'text_14'  => $text_14,
					   'text_15'  => $text_15,
					   'text_16'  => $text_16,
					   'text_17'  => $text_17,
					   'text_18'  => $text_18,
					   'text_19'  => $text_19,
					   'text_20'  => $text_20,
					   'text_21'  => $text_21,
					   'text_22'  => $text_22,
					   'text_23'  => $text_23,
					   'text_24'  => $text_24,
					   'text_25'  => $text_25,
					   'text_26'  => $text_26,
					   'text_27'  => $text_27,
					   'text_28'  => $text_28,
					  
					   'text_36'  => $text_36,
					   'text_37'  => $text_37,
					   'text_38'  => $text_38,
				
				   );
			$condition = array(
                   'id'  => $id
               );
			$this->commonmodel->update_entry('tbl_footerlinks',$condition,$newdata);
		
		
	
		redirect(base_url().'admin/footerlinks');
		
	}
	
	
	public function edit()
	{
		$result['page']="footerlinks";
		$id=1;
		$condition = array(
                   'id'  => $id
               );
		$result['footerlinks']=$this->commonmodel->select($condition,'tbl_footerlinks');
		$this->template->load('template', 'footerlinks',$result);
	}
	
}

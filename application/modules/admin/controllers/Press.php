<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['presss']=$this->commonmodel->select(NULL,'tbl_press');
		$result['page']="listpress";
		$this->template->load('template', 'listpress',$result);
		
	}
	public function add()
	{
		
		$result['page']="addpress";
		$this->template->load('template', 'addpress',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'press_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_press',$condition);
			   redirect(base_url().'admin/press');
	}
	public function insert()
	{
		if($this->input->post('pressid')!=NULL)
		{
			$id=$this->input->post('pressid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			
			$pdate=date('Y-m-d',strtotime($this->input->post('pressdate')));
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."press/".$file_name;
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   'press_description'  => $description,
					   'press_title'  => $title,
					    'press_date'  => $pdate,
					   'press_image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   'press_description'  => $description,
					    'press_date'  => $pdate,
					   'press_title'  => $title,
				   );

			}
			$condition = array(
                   'press_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_press',$condition,$newdata);
		}
		else
		{
		
			$id=$this->input->post('pressid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$pdate=date('Y-m-d',strtotime($this->input->post('pressdate')));
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."press/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'press_description'  => $description,
					   'press_title'  => $title,
					   'press_date'  => $pdate,
					   'press_image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_press');
		}
	
		redirect(base_url().'admin/press');
		
	}
	
	
	public function edit()
	{
		$result['page']="press";
		$id=$this->uri->segment('4');
		$condition = array(
                   'press_id'  => $id
               );
		$result['press']=$this->commonmodel->select($condition,'tbl_press');
		$this->template->load('template', 'addpress',$result);
	}
	
}

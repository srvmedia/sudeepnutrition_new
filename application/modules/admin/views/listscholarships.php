
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Scholarship Enquiries List</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>Scholarship Enquiries List</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
 <div class="row">
    <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
<table id="customers2" class="table datatable">
        <thead>
            <tr>
                <th>No</th>
                <th>Course Name</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Percentage</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
        <?php $i=0;
	foreach($list['enquiries'] as $row)
		{
		$i++;
		?>
       <tr>
       <td><?php echo $i?></td>
       <td><?php echo $row["course_name"];?></td>
       <td><?php echo $row["name"];?></td>
       <td><?php echo $row["email_from"];?></td>
       <td><?php echo $row["phone"];?></td>
       <td><?php echo $row["percentage"];?></td>
       <td><?php  echo date('d-F-Y', strtotime($row["date_of_apply"]));?></td>
                                                    
    </tr>
    <?php } ?>
     </tbody>
     </table>                   
                                </div>
                
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    

        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->        
        
        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
      







<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Media</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/media/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Media</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Media Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['media'])){ echo $list['media'][0]['media_title']; }?>" class="form-control" name="title" id="title"/>                                                                      
                                                <input type="hidden" value="<?php if(isset($list['media'])){ echo $list['media'][0]['media_id']; }?>" class="form-control" name="mediaid" id="mediaid"/>   
                                                     <input type="hidden" value="" class="form-control" name="description" id="description"/>      
                                        </div>
                                    </div>
                              <?php /*?>  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Media Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                                  
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['media'])){ echo $list['media'][0]['media_description']; }?></textarea>
                                        </div>
                                    </div>
                                </div><?php */?>
                                <?php if(isset($list['media'])){
									?>
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Media File</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                          <input type="file" class="fileinput"   name="image" />
                                                                
                                                        </div>
                                                    </div>
													<?php
								}
								else
								{
									?>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Media File</label>
                                                        <div class="col-md-6 col-xs-12">   
                                                              <input type="file" class="fileinput"  required="required"  name="image" />
                                                                 
                                                        </div>
                                                    </div>
                                    <?php
								}
								?>
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Media</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/homehostel/update" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Media</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label"></label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                          <p><img src="<?php echo base_url()?>assets/images/<?php echo $list['hostel'][0]['image'];?>" width="250" height="250"></p>
                                                          <p><?php echo $list['hostel'][0]['title'];?></p>
                                                        </div>
                                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Hostel Link Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                               
                                                  
                                                     <input type="text" value="<?php echo $list['hostel'][0]['title'];?>" class="form-control" required="required" name="title"/>      
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Hostel Link</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                               
                                                  
                                                     <input type="text" value="<?php echo $list['hostel'][0]['link'];?>" class="form-control" name="link" />      
                                        </div>
                                    </div>
                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Media File</label>
                                                        <div class="col-md-6 col-xs-12">   
                                                              <input type="file" class="fileinput" name="image" />
                                                                 
                                                        </div>
                                    </div>
                                 
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
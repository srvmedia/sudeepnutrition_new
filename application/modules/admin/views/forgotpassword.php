<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>T John College</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icoeeen" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url()?>assets/admin_css/theme-default.css"/>
        <?php /*?><link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/upload_js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" /><?php */?>
        
        <!-- EOF CSS INCLUDE -->                                    
    </head>
                
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                          
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/forgotpassword/update" method="post" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Change</strong> Password</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                        <span style="color:#F00"><?php echo $this->session->flashdata('message');?></span>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Username:</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                            
                                           <input type="text" value="" required class="form-control" name="uname" id="uname"/>          
                                                                               
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Old Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                             
                                                <input type="password" required value="" class="form-control" name="oldpassword" id="oldpassword"/>                                         
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">New Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                             
                                                <input type="password" required value="" class="form-control" name="password" id="password"/>                                         
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                             
                                                <input type="password" value="" class="form-control" name="cpassword" id="cpassword"/>                                         
                                        </div>
                                    </div>
                                     
                                </div>
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
         <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo base_url()?>admin/login/logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->

        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
   <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script> -->
   <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/jsfunctions.js"></script>   
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
        
       
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
                     
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
                        
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
       <!-- <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/settings.js"></script> -->
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/actions.js"></script>
        
        <!--<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/demo_dashboard.js"></script>-->
        <!-- END TEMPLATE -->
        
         <!-- JS for form lements  -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-select.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tagsinput/jquery.tagsinput.min.js"></script> -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/dropzone/dropzone.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/fileinput/fileinput.min.js"></script> 
         <!-- END forms  -->
         
          <!-- Data Data table   -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/tableExport.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jquery.base64.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/html2canvas.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/jspdf.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/libs/base64.js"></script> 
         <!-- END Data table  -->
          <!-- validation   -->  
        <!--<script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/validationengine/jquery.validationEngine.js'></script> -->       
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/jquery-validation/jquery.validate.js'></script> 
          <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/maskedinput/jquery.maskedinput.min.js'></script>
         <!-- END  -->
           <!-- Editor   -->  
       <!--  <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/summernote/summernote.js"></script> -->  
		<script type="text/javascript" src="<?php echo base_url()?>assets/editor/nicEdit.js"></script>
         <!-- END  -->
         <!-- Upload file   --> 
           <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
   <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>-->
   
   
  <!-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
         <script type="text/javascript" src="<?php echo base_url()?>assets/upload_js/plupload.full.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/upload_js/jquery.ui.plupload/jquery.ui.plupload.js"></script>-->
        <!-- END  -->
 
</body>
</html>
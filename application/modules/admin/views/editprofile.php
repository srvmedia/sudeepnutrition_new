<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Edit</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/editadminprofile/update" method="post" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Edit</strong> Profile</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Username:</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                            
                                            <?php if(isset($list['details'])){ echo $list['details'][0]['admin_username']; }?>
                                                                               
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Display Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                         
                                                <input type="text" value="<?php if(isset($list['details'])){ echo $list['details'][0]['admin_dname']; }?>" class="form-control" name="dname" id="dname"/>                                         
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                             
                                                <input type="password" value="" class="form-control" name="password" id="password"/>                                         
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                             
                                                <input type="password" value="" class="form-control" name="cpassword" id="cpassword"/>                                         
                                        </div>
                                    </div>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Profile Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image" id="file-simple" />        
                                                        </div>
                                     </div>
                                </div>
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
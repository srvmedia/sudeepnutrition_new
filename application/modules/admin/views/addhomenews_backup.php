<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Placement</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/homenews/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Home News </strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                   
                                  
                                   
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['homenews'])){ echo $list['homenews'][0]['homenewsevents_id']; }?>" class="form-control" name="homenewseventsid" id="homenewseventsid"/>                                                                      
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['homenews'])){ echo $list['homenews'][0]['homenewsevents_content']; }?></textarea>
                                        </div>
                                    </div>
                                </div>
                           
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
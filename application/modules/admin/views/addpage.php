<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Site Page</a></li>
                    <li><a href="#">Edit</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/page/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Site Page</strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                      <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Page Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                  
                                                 <input type="text" class="form-control" required="required" rows="5"  name="pname" value="<?php if(isset($list['pagelist'])){ echo $list['pagelist'][0]['page_name']; }?>">                                                              
                                             
                                        </div>
                                    </div>
                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Parent page</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select"  name="parent" id="parent">
                                                              
                                                                 <option value="0">Main</option>
                                                                <?php
																
                                                                foreach($list['allpages'] as $pagevalval)
                                                                {
																	if($list['pagelist'][0]['page_id']!=$pagevalval['page_id'])
																	{
                                                                    ?>
                                                                        <option value="<?php echo $pagevalval['page_id']?>" <?php if(@$list['pagelist'][0]['page_parent']== $pagevalval['page_id']){?> selected="selected" <?php } ?>><?php echo $pagevalval['page_name'];?></option>
                                                                <?php
                                                                }
																}
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Banner Image</label>
                                                        <div class="col-md-6 col-xs-12">  
                                                        <?php if(isset($list['pagelist'])){
															?>                                          
                                                            <input type="file"  name="image" id="file-simple" /> Please uplaod an image with resolution 750 X 350
                                                            <?php
														}
														else
														{
														?>
                                                         <input type="file"  name="image" id="file-simple" />  Please uplaod an image with resolution 750 X 350
                                                         <?php
														}
														
														?>
                                                        
                                                        </div>
                                                    </div>
                                                <div class="form-group">
                                                                        <label class="col-md-3 col-xs-12 control-label">Want to show in Menu List</label>
                                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                                            <select class="form-control select"  name="asmenu" id="asmenu">
                                                                                 <option value="1" <?php if(@$list['pagelist'][0]['page_showinmenu']== 1){?> selected="selected" <?php }?> >Yes</option>
                                                                                 <option value="0" <?php if(@$list['pagelist'][0]['page_showinmenu']== 0){?> selected="selected" <?php }?>>No</option>
                                                                                
                                                                            </select>
                                                                        </div>
                                                 </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Want to show sidebar in this page</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select"  name="hassidemenu" id="hassidemenu">
                                                                 <option value="1" <?php if(@$list['pagelist'][0]['page_hassidemenu']== 1){?> selected="selected" <?php }?> >Yes</option>
                                                                 <option value="0" <?php if(@$list['pagelist'][0]['page_hassidemenu']== 0){?> selected="selected" <?php }?>>No</option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Page Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['pagelist'])){ echo $list['pagelist'][0]['page_id']; }?>" class="form-control" name="pageid" id="pageid"/>        
                                                 <textarea class="form-control tinymceeditor" rows="5"  name="content"> <?php if(isset($list['pagelist'])){ echo $list['pagelist'][0]['page_content']; }?></textarea>                                                              
                                             
                                        </div>
                                    </div>
                                    <div class="form-group addmoresection">
                                    <?php
									$i=0;
									
									foreach($list['pagetab'] as $tabval)
									{
										$i++;
										?>
                                    <div style="margin-bottom:15px;"><div class="form-group"><label class="col-md-3 col-xs-12 control-label">Tab Title</label>
                                    <div class="col-md-6 col-xs-12"><input type="text" class="form-control" value="<?php  echo $tabval['pagetab_title'];?>" name="tabtitle[]" required="required"></div></div><div class="form-group"><label class="col-md-3 col-xs-12 control-label">Tab Content</label><div class="col-md-6 col-xs-12"><textarea class="form-control tinymceeditor" rows="5"  name="tabcontent[]"><?php  echo $tabval['pagetab_content'];?> </textarea></div><a href="#" class="remove_field"><img src="http://www.codestager.com/OTHTJNWEDG/cms/assets/images/remove.png" title="Delete"></a></div></div>
                                    <?php
									}
									?>
                                    </div>
                                    
                                    
                                    <input type="hidden" class="form-control" name="total"  id="total" value="<?php  echo $i;?>">
                                    
                                     <div class="form-group">
                                     <div class="col-md-3 col-xs-12"></div>
                                       <div class="col-md-6 col-xs-12"><button class="btn btn-success add_field_button" >Add Tab</button></div>
                                      </div>
                                </div>
                               
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
       
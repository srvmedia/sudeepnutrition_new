<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home Slider</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/homeslider/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Home Slider</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Slider Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['slider'])){ echo $list['slider'][0]['homeslider_title']; }?>" class="form-control" name="title" id="title"/>                                                                      
                                              <input type="hidden" value="<?php if(isset($list['slider'])){ echo $list['slider'][0]['homeslider_id']; }?>" class="form-control" name="sliderid" id="sliderid"/>  
                                              <input type="hidden" value="" class="form-control" name="description" id="description"/>                                                                      
                                               
                                        </div>
                                    </div>
                               <?php /*?> <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Slider Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['slider'])){ echo $list['slider'][0]['homeslider_id']; }?>" class="form-control" name="sliderid" id="sliderid"/>                                                                      
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['slider'])){ echo $list['slider'][0]['homeslider_description']; }?></textarea>
                                        </div>
                                    </div><?php */?>
                               
                                  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Title Link</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                           <input  class="form-control input-lg" required="required"  name="pageid" id="pageid" value="<?php echo @$list['slider'][0]['homeslider_page']; ?>"/>
                                        </div>
                                    </div>
                                
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Slider Image</label>
                                                        <div class="col-md-6 col-xs-12">  
                                                        <?php if(isset($list['slider'])){
															?>                                          
                                                            <input type="file"  name="image" id="file-simple" /> 
                                                            <?php
														}
														else
														{
														?>
                                                         <input type="file" required="required" name="image" id="file-simple" /> 
                                                         <?php
														}
														
														?>
                                                        
                                                        </div>
                                                    </div>
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
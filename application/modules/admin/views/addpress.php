<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Press</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/press/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Press</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Published News Paper</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['press'])){ echo $list['press'][0]['press_title']; }?>" class="form-control" name="title" id="title"/>                                                                      
                                                <input type="hidden" value="<?php if(isset($list['press'])){ echo $list['press'][0]['press_id']; }?>" class="form-control" name="pressid" id="pressid"/>   
                                                     <input type="hidden" value="" class="form-control" name="description" id="description"/>      
                                        </div>
                                    </div>
                                     <div class="form-group">
                                    
                                        <label class="col-md-3 col-xs-12 control-label">Published  Date</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['press'])){ echo date('d-m-Y',strtotime($list['press'][0]['press_date'])); }?>" class="form-control datepicker"  name="pressdate" required="required" id="pressdate"/>                                                                      
                                              
                                        </div>
                                    </div>
                              <?php /*?>  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Press Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                                  
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['press'])){ echo $list['press'][0]['press_description']; }?></textarea>
                                        </div>
                                    </div>
                                </div><?php */?>
                                <?php if(isset($list['press'])){
									?>
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Press Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image"  id="file-simple" />        
                                                        </div>
                                                    </div>
													<?php
								}
								else
								{
									?>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Press Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image" required="required" id="file-simple" />        
                                                        </div>
                                                    </div>
                                    <?php
								}
								?>
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
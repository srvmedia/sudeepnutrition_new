
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/dashboard">Home</a></li>
                    <li class="active">Contact Address</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Contact Address</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                                                        
                                    
                                </div>
                                <div class="panel-body">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SI No</th>
                                                <th>Corporate Address</th>
                                                <th>Campus Address</th>
                                                 <th>Phone</th>
                                                  <th>Email</th>
                                                
                                                 <th></th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
										$i=0;
										foreach($list['contactaddress'] as $val)
										{
											$i++;
											?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                <td><?php echo $val['contactaddress_address']?></td>
                                                <td><?php echo $val['contactaddress_campusaddress']?></td>
                                                <td><?php echo $val['contactaddress_phone']?></td>
                                                <td><?php echo $val['contactaddress_email']?></td>
                                                
                                                <td><a href="contactaddress/edit/<?php echo $val['contactaddress_id']?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a></td>
                                                
                                            </tr>
                                            <?php
										}
										?>
                                        </tbody>
                                    </table>                                    
                                    
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    

        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->        
        
        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
      







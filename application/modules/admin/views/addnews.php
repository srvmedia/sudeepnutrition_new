<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">News</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form   onsubmit="return validate();" id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/news/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>News</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                    
                                        <label class="col-md-3 col-xs-12 control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['news'])){ echo $list['news'][0]['news_title']; }?>" class="form-control" name="title" required="required" id="title"/>                                                                      
                                              
                                        </div>
                                    </div>
                                      <div class="form-group">
                                    
                                        <label class="col-md-3 col-xs-12 control-label">Sort Order</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="number" value="<?php if(isset($list['news'])){ echo $list['news'][0]['news_sortorder']; }?>" class="form-control" name="sortorder" required="required" id="sortorder"/>                                                                      
                                              
                                        </div>
                                    </div>
                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Featured(Show in Home page)</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select"  name="featured" id="featured">
                                                                 <option value="1" <?php if(@$list['news'][0]['news_featured']== 1){?> selected="selected" <?php }?> >Yes</option>
                                                                 <option value="0" <?php if(@$list['news'][0]['news_featured']== 0){?> selected="selected" <?php }?>>No</option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Short Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                              
                                               <textarea required="required"  class="form-control" id="shortcontent" rows="5" name="shortcontent"><?php if(isset($list['news'])){ echo $list['news'][0]['news_shortcontent']; }?></textarea>
                                                                
                                               
                                               
                                        </div>
                                    </div>
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['news'])){ echo $list['news'][0]['news_id']; }?>" class="form-control" name="newsid" id="newsid"/>                                                                      
                                               <textarea  class="form-control tinymceeditor" id="content" rows="5" name="content"><?php if(isset($list['news'])){ echo $list['news'][0]['news_content']; }?></textarea>
                                                <label id="content-error" class="test" for="content" style="display:none;color:#E04B4A;">This field is required.</label>                                                   
                                               
                                               
                                        </div>
                                    </div>
                                </div>
                       
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
          <script>
        function validate()
{ 

    var nicInstance = nicEditors.findEditor('content');
    var messageContent = nicInstance.getContent();

    if(messageContent=="<br>" || messageContent=="" ) { 
   // document.jvalidate.content.focus();
	
	document.getElementById('content-error').style.display = 'block';
     return false;
   }
   else {
        document.getElementById('content-error').style.display = 'none';
   }   
    return true;
} 
</script>    
 <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo base_url()?>admin/login/logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->

        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
   <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script> -->
   <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/jsfunctions.js"></script>   
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
        
       
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
                     
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
                        
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
       <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/settings.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/actions.js"></script>
        
        <!--<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/demo_dashboard.js"></script>-->
        <!-- END TEMPLATE -->
        
         <!-- JS for form lements  -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-select.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tagsinput/jquery.tagsinput.min.js"></script> -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/dropzone/dropzone.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/fileinput/fileinput.min.js"></script> 
         <!-- END forms  -->
         
          <!-- Data Data table   -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/tableExport.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jquery.base64.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/html2canvas.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/jspdf.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/libs/base64.js"></script> 
         <!-- END Data table  -->
          <!-- validation   -->  
        <!--<script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/validationengine/jquery.validationEngine.js'></script> -->       
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/jquery-validation/jquery.validate.js'></script> 
          <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/maskedinput/jquery.maskedinput.min.js'></script>
         <!-- END  -->
           <!-- Editor   -->  
       <!--  <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/summernote/summernote.js"></script> -->  
		<?php /*?><script type="text/javascript" src="<?php echo base_url()?>assets/editor/nicEdit.js"></script><?php */?>
 <script  type='text/javascript' src='<?php echo base_url()?>assets/admin_js/tinymce.min.js'></script>      
  <?php /*?><script src="//cdn.tinymce.com/4/tinymce.min.js"></script><?php */?>
  <script>tinymce.init({
  mode : "specific_textareas",
        editor_selector : "tinymceeditor",
  height: 500,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
  
});</script>
         <!-- END  -->
         <!-- Upload file   --> 
           <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
   <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>-->
   
   
  <!-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
         <script type="text/javascript" src="<?php// echo base_url()?>assets/upload_js/plupload.full.min.js"></script>
		<script type="text/javascript" src="<?php// echo base_url()?>assets/upload_js/jquery.ui.plupload/jquery.ui.plupload.js"></script>-->
        <!-- END  -->
 
</body>
</html>
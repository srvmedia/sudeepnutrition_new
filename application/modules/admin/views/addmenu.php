<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>/admin/menu">Menu-List</a></li>
                    <li><a href="#">Edit</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/menu/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Site Menu</strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                     
                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Menu</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select" required="required"  name="pageid" id="pageid">

                                                                <?php
																$menuidarray=array();
																foreach($list['allmenupriority'] as $idval)
                                                                {
																	array_push($menuidarray,$idval['menu_page_id']);
																}
																
                                                                foreach($list['allpages'] as $pagevalval)
                                                                {
																	if (!in_array($pagevalval['page_id'], $menuidarray) || $list['menulist'][0]['menu_page_id']==$pagevalval['page_id'] ) {
                                                                    ?>
                                                                        <option value="<?php echo $pagevalval['page_id']?>" <?php if(@$list['menulist'][0]['menu_page_id']== $pagevalval['page_id']){?> selected="selected" <?php } ?>><?php echo $pagevalval['page_name'];?></option>
                                                                <?php
																}
																}
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                 <div class="form-group">
                                 
                                        <label class="col-md-3 col-xs-12 control-label">Position</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                  <input type="hidden" value="<?php if(isset($list['menulist'])){ echo $list['menulist'][0]['menu_id']; }?>" class="form-control" name="menuid" id="menuid"/> 
                                                  <select class="form-control select" required="required"  name="priority" id="priority">

                                                                <?php
																$priorityarray=array();
																foreach($list['allmenupriority'] as $prval)
                                                                {
																	array_push($priorityarray,$prval['menu_priority']);
																}
																
                                                                for($i=2;$i<=6;$i++)
                                                                {
																	if (!in_array($i, $priorityarray) || $list['menulist'][0]['menu_priority']==$i ) {
																
                                                                    ?>
                                                                        <option value="<?php echo $i;?>" <?php if(@$list['menulist'][0]['menu_priority']== $i){?> selected="selected" <?php } ?>><?php echo $i;?></option>
                                                                <?php
																	}
																}
                                                                ?>
                                                                
                                                            </select>       
                                                                                                         
                                             
                                        </div>
                                    </div>
                                    
                                </div>
                                <?php if(isset($list['level1'])){ ?>
                             <div class="panel-body">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SI No</th>
                                                <th>Page Name</th>
                                                <th>Sort Order</th>
                                                <th>Sub-Menu</th>
                                           </tr>
                                        </thead>
                                        <tbody>
                                        <?php
					$i=0;
					foreach($list['level1'] as $val)
					{
					$i++; ?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                <td><?php echo $val['page_name']?></td>
                                                <td><input type="hidden" name="page_id[]" value="<?php echo $val['page_id']?>" /> <input name="sortorder[]" value="<?php echo $val['sort_order']?>"/></td>
                                                 <td><a href="<?php echo base_url(); ?>admin/menu/submenu/<?php echo $list['menu_id']?>/<?php echo $val['page_id']?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a></td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>                                    
                                    
                                </div>
                                <?php } ?>
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        
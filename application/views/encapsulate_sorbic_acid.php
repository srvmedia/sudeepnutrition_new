<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/sorbicacid.css">
    <!-- <link rel="stylesheet" href="styles/jon_our_team.css"> -->
</head>



<!-- Humberger Navbar -->


<!-- Banner -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?><?php echo $main_header[0]->image; ?>"
                        class="img-fluid w-100 d-md-block d-none">
                    <img src="<?php echo base_url(); ?><?php echo $main_header[0]->mobile_image; ?>" class="img-fluid w-100 d-block d-lg-none">
                    <div class="banner_text">
                        <h3 class="text-white"><?php echo $main_header[0]->sub_section_header; ?></h3>
                        <div class="primary-button pt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Section 1 -->

<section class="content1 pt-5 pb-5 ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h5 class="color-green"><?php echo $product_content[0]->header1; ?></h5>
            </div>
            <div class="col-md-6">
                <h5 class="color-grey"><?php echo $product_content[0]->header2; ?></h5>
            </div>
        </div>
    </div>
</section>


<!-- section 1 end -->


<!-- section 2 -->
<?php if($sub_tab==9) : ?>

    <section class="how_encapsulation pt-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="color-blue text-center">This is how encapsulation technology and encapsulated ingredients
                    plays an important role</h5>
            </div>
        </div>
        <div class="col-lg-12 pt-5">
            <div class="row">
                <div class="col-lg-2">
                    <div class="row">
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="row">
                        <div class="col-lg-12 encap-div">
                            <img src="<?php echo base_url(); ?>assets/images/Assets/Product detail page- Encapsulated Sorbic acid/1.svg" class="img-fluid img-bg">
                            <p class="text-p">Microencapsulation enables a controlled release of the active material,
                                which basically depends on the kind of coating.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="row">
                        <div class="col-lg-12 encap-div">
                            <img src="<?php echo base_url(); ?>assets/images/Assets/Product detail page- Encapsulated Sorbic acid/2.svg" class="img-fluid img-bg">
                            <p class="text-p">Temperature-dependent- In bakery applications, the breakdown of sorbic
                                acid is achieved only during baking temperature.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endif; ?>
<!-- section 2 end -->

<!-- section 3 -->

<section class="mission pt-5 pb-5">
    <div class="container">
        <?php if($product_content[0]->why_it_works) : ?>
        <div class="row">
            <div class="col-md-6">
                <h5 class="color-blue">Why it works?</h5>
                <?php echo $product_content[0]->why_it_works; ?>

            </div>
            <div class="col-md-6">
                <img src="<?php echo base_url(); ?><?php echo $product_content[0]->why_it_work_image; ?>" alt="" class="img-fluid section1">
            </div>
        </div>
        <?php endif; ?>
        <?php if($product_content[0]->How_it_works) : ?>
        <div class="row">
            <div class="col-md-6">
                <h5 class="color-blue padd-p">How it works?</h5>
                <?php echo $product_content[0]->How_it_works; ?>

            </div>
            <div class="col-md-6 px-0 col-section">
                <img src="<?php echo base_url(); ?><?php echo $product_content[0]->why_it_work_image; ?>" alt="" class="img-fluid section1">
            </div>
        </div>
        <?php endif; ?>
        <?php $flag=0; ?>
        <?php if($product_content[0]->mechanism_of_action) : ?>
            <?php $flag=1; ?>
        <div class="row">
            <div class="col-md-6">
                <img src="<?php echo base_url(); ?><?php echo $product_content[0]->mechanism_of_action_img; ?>" alt="" class="img-fluid section1">
            </div>
            <div class="col-md-6">
                <h5 class="color-blue pt-4">Mechanism of Action</h5>
                <?php echo $product_content[0]->mechanism_of_action; ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="row">
        <?php if($flag==0) : ?>
            <div class="col-md-6">
                <img src="<?php echo base_url(); ?><?php echo $product_content[0]->advantages_img; ?>" alt="" class="img-fluid section1">
            </div>
            <?php endif; ?>
            <div class="col-md-6">
                <h5 class="color-blue pt-4">Advantages</h5>
                <ul class="st1 listicon">
                <?php foreach ($advantages as $advantage) { ?>
                    <li><img src="<?php echo base_url(); ?>assets/images/Assets/Product detail page- Encapsulated Sorbic acid/bullet.svg" alt="">
                    <?php echo $advantage->advantage_name; ?></li>
                <?php } ?>
                </ul>
            </div>
            <?php if($flag==1) : ?>
            <div class="col-md-6">
                <img src="<?php echo base_url(); ?><?php echo $product_content[0]->advantages_img; ?>" alt="" class="img-fluid section1">
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>


<section class="application mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="app text-center pt-5">Application</h5>
            </div>
            <?php foreach ($applications as $application) { ?>
        <div class="col-md-2 encap-pdd encap-div encap">
            <img src="<?php echo base_url(); ?><?php echo $application->image; ?>" alt="" class="img1_bg">
            <p class="application_text"><?php echo $application->application_name; ?></p>
        </div>
        <?php } ?>
        </div>
    </div>
</section>

</body>

</html>
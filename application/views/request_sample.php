<style type="text/css">

.form_div {
    padding: 1.5rem 3rem;
}

.requestsample_form {
    /* background: url(); */
    /* background-repeat: no-repeat;
    background-size: cover; */
    background-color:black;
}

.banner_form {
    position: relative;
    padding: 3rem;
    width: 55rem;
    z-index: 999;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    box-shadow: 0px 16px 32px #00000017;
    border-radius: 10px;
    margin: 4rem 0;
}

.form_img {
    position: absolute;
    top: 0;
    right: 0;
}

.form-title h4 {
    font-weight: bold;
    margin: 10px 0;
    padding-left: 0px;
    color: #2C5B8D;
    font-size: 2.5rem;
}

.form-group label {
    font-size: 14px;
    color: #444;
    text-align: left;
    display: block;
    margin-bottom: 5px;
}

p.p1text {
    font-size: 1.3rem;
}

.form-control {
    text-align: left;
    background-color: transparent;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    border-top: none;
    border-right: 0;
    border-left: 0;
    border-bottom: 1px solid #eee;
    color: #888!important;
    box-shadow: none;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    padding-left: 5px;
}

.form-control:focus {
    color: #495057;
    background-color: #fff;
    border-color: #80bdff;
    outline: 0;
    border-top: none;
    border-right: 0;
    border-left: 0;
    box-shadow: none;
}

.btn_sec {
    color: white;
    display: inline-block;
    background-color: #f7972d;
    border-radius: 2rem;
    /* padding: 1rem; */
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    margin-bottom: 2rem;
    margin-left: 1rem;
}

.form_div {
    background-color: #F9F9F9;
}

/* p.ptext {
    margin-left: 1rem;
} */

@media screen (min-width:601px) and (max-width:768px) {
    .banner_form {
     width: 100%;
    }
}

@media screen  and (max-width:500px) {
    .banner_form {
        width: 100%;
        padding: none;
        margin: 0;
    }
    .form-title h4 {
        font-size: 1.5rem;
    }
    .banner_form {
        box-shadow: none;
        border-radius: 0;
        margin: 0;
    }
}

@media screen and (min-width:437px) and (max-width:600px) {
    .banner_form {
        padding: 1rem;
        width: 26rem;
    }
    .form-title h4 {
        font-size: 2rem;
    }
}
</style>

<!-- Humberger Navbar -->


<section class="requestsample_form sample">
    <div class="container sample">
        <div class="row justify-content-center">
                <div id="slider_banner" class="carousel slide position-relative" data-ride="carousel">
                    <div class="banner_form">
                        <div class="banner-form">
                            <div class="form-title">
                                <h4 class="text-uppercase">Request a Sample</h4>
                                <p class="p1text">We make sure we send you the most appropriate samples, hence we require a certain
                                    level of
                                    information which will enable us to recommend the most appropriate encapsulated
                                    ingredients
                                    for your finished product.</p>
                            </div>
                            <form name="register-form">
                                <div class="form_div">
                                    <div class="">
                                        <div id="allerror" class="font-weight-bold text-danger mb-2"></div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Full Name*" id="name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Email Address*"
                                                id="email">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control"
                                                        placeholder="Contact Number*" id="mobile">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="applications" id="applications">
                                                        <option value="" selected="selected">Country of origin*</option>
                                                        <option value="Concrete Admixtures">Concrete Admixtures</option>
                                                        <option value="Surface Treatments">Surface Treatments</option>
                                                        <option value="Grouts and Anchors">Grouts and Anchors</option>
                                                        <option value="Repair and Rehabilitation">Repair and
                                                            Rehabilitation
                                                        </option>
                                                        <option value="Protective Coatings">Protective Coatings</option>
                                                        <option value="Industrial Flooring">Industrial Flooring</option>
                                                        <option value="Water Proofing">Water Proofing</option>
                                                        <option value="Sealants">Sealants</option>
                                                        <option value="Adhesives">Adhesives</option>
                                                        <!--  <option value="Cement Grinding Aids">Cement Grinding Aids</option> -->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Company Name*"
                                                        id="companyname">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Designation*"
                                                        id="companyname">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group mb-2">
                                            <label>What Ingredient you are looking for?</label>
                                        </div>
                                        <div class="form-check mb-3 form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="category"
                                                            value="Architects and Consultants">Encapsulated Ingredients
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="category"
                                                            value="Architects and Consultants">Spray Dried Ingredients
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="category"
                                                            value="Architects and Consultants">Other
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="category"
                                                            value="Applicators">
                                                        Custom Nutrient Blends
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="category"
                                                            value="Applicators">
                                                        Granulated Products
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            placeholder="Other products*" id="email">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group mb-2">
                                            <label>What product are you looking to develop?</label>
                                        </div>
                                        <div class="form-group mb-3">
                                            <input type="text" class="form-control" placeholder="Enter your message"
                                                id="requirement">
                                        </div>
                                        <p class="ptext">Sudeep Nutrition Pvt Ltd. is committed to protecting and
                                            respecting your privacy. We
                                            will only use your details for the purpose of enquiry, we will never share
                                            your
                                            details with the third parties. Please see our <a
                                                href="#" class="clr_1">Privacy Policy</a> for more
                                            information.
                                        </p>
                                        <!-- <div class="mb-2">
                                    <input class="btn" id="submit_btn" type="button" value="SUBMIT">
                                </div> -->
                                        <a href="#home" class="btn_sec">Submit <span>→</span></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>

</section>
<style type="text/css">
.form_div {
    padding: 0;
}

.requestsample_form {
    background: url('./assets/images/Product Information service/banner.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    /* background-color: black; */
}

.banner_form {
    position: relative;
    padding: 3rem;
    width: 45rem;
    z-index: 999;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    box-shadow: 0px 16px 32px #00000017;
    border-radius: 10px;
    margin: 4rem 0;
}

.form_img {
    position: absolute;
    top: 0;
    right: 0;
}

.form-title h4 {
    font-weight: bold;
    margin: 10px 0;
    padding-left: 0px;
    color: #2C5B8D;
    font-size: 2.5rem;
}

.form-group label {
    font-size: 14px;
    color: #444;
    text-align: left;
    display: block;
    margin-bottom: 5px;
}

p.p1text {
    font-size: 1.3rem;
}

.form-control {
    text-align: left;
    background-color: transparent;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    border-top: none;
    border-right: 0;
    border-left: 0;
    border-bottom: 1px solid #eee;
    color: #888 !important;
    box-shadow: none;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    padding-left: 5px;
}

.form-control:focus {
    color: #495057;
    background-color: #fff;
    border-color: #80bdff;
    outline: 0;
    border-top: none;
    border-right: 0;
    border-left: 0;
    box-shadow: none;
}

.btn_sec {
    color: white;
    display: inline-block;
    background-color: #f7972d;
    border-radius: 2rem;
    /* padding: 1rem; */
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    margin-bottom: 2rem;
    margin-left: 1rem;
}

/* .form_div {
    background-color: #F9F9F9;
} */

p.ptext {
    margin-left: 1rem;
    font-size: 0.9rem;
}

.btn_sec {
        margin:0 0 0.8rem 0;
    }

@media screen (min-width:601px) and (max-width:768px) {

    .banner_form {
        width: calc(100% - 30px);
        margin: 0 15px;
    }
    .requestsample_form {
        background: transparent;
    }
}

@media screen and (max-width:436px) {
    .banner_form {
        width: calc(100% - 30px);
        padding: 1rem;
        margin: 0rem 15px;
    }
    .requestsample_form {
        background: transparent;
    }

    .form-title h4 {
        font-size: 1.3rem;
    }

    p.p1text {
        font-size: 1.15rem !important;
    }
}

@media screen and (min-width:437px) and (max-width:600px) {
    
    .banner_form {
        width: calc(100% - 30px);
        padding: 1rem;
        margin: 0rem 15px;
    }
    .requestsample_form {
        background: transparent;
    }

    .form-title h4 {
        font-size: 2rem;
    }
}


@media screen and (min-width: 768px) and (max-width: 993px){
    .banner_form {
        width: 30rem;
    }
}
</style>

<!-- Humberger Navbar -->


<section class="requestsample_form mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div id="slider_banner" class="carousel slide position-relative" data-ride="carousel">
                <div class="banner_form">
                    <div class="banner-form">
                        <div class="form-title">
                            <h4 class="text-uppercase">Product Information</h4>
                            <p class="p1text mb-4">Reach out to us in order to get information, guidance, opinions about our
                                products in order meet your challenges from our technical team with professional
                                expertise.</p>
                        </div>
                        <form name="register-form">
                            <div class="form_div">
                                <div class="">
                                    <div id="allerror" class="font-weight-bold text-danger mb-2"></div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                            placeholder="Sudeep nutrition product name *" id="name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                            placeholder="Estimated monthly requirement *" id="requirement">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                            placeholder="Time duration for us to develop the solution *" id="duration">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Contact Number*"
                                                    id="mobile">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control"
                                                        placeholder="email address *" id="email">
                                                </div>
                                        </div>
                                    </div>
                                    <p class="ptext">Sudeep Nutrition Pvt Ltd. is committed to protecting and
                                        respecting your privacy. We
                                        will only use your details for the purpose of enquiry, we will never share
                                        your
                                        details with the third parties. Please see our <a style="color:#36A2FF"
                                            href="#">Privacy Policy</a> for more
                                        information.
                                    </p>
                                    <a href="#home" class="btn_sec">Submit <span>→</span></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
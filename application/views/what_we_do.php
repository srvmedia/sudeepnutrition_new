

<style type="text/css">

 .application11{
        background-color: #FBFBFB;
  }
  .padd{
  padding: 5rem 0rem;
  }
    p.text-black02 {
        font-weight: normal;
    }

    .text-partner {
        font-size:3.5rem;
    }

    .mt-5 {
        margin-top: 5rem!important;
    }

    @media screen and (max-width:767px) {
    .text-partner {
        font-size: 1.8rem;
    }

    .mt-5 {
        margin-top: 1rem!important;
    }

    }


    @media screen and (max-width: 767px) {
     .banner_text h3 {
    font-size: 1.75rem !important;
  }
}

</style>



<!-- Humberger Navbar -->


<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
        <img src="<?php echo base_url(); ?>assets/images/what/What we do banner.jpg" class="img-fluid w-100 d-md-block d-none">
        <img src="<?php echo base_url(); ?>assets/images/what/mb_wahtwedo.jpg" class="img-fluid w-100 d-block d-lg-none">
        <div class="banner_text">
            <div class="primary-button pt-3">
            </div>
            <h3 class="text-white">What we do</h3>
            </div>
            </div>
            </div>
            </div>
         </div>
</section>
<!--====== banner end ==========-->


<section class="microencap mt-5">
    <div class="container-fluid container-custom">
        <div class="row">
            <div class="col-lg-6 col-12">
                <p class="text-blue02">Microencapsulation technology has garnered a lot of importance around the globe. It is a cost-effective solution that has found its niche in every sector of industry and its use has been increasing in a plethora of applications.</p>
                <p class="text-black02">Accredited with FSSC 22000, WHO GMP, ISO 9001:2015, HACCP, Kosher, and Halal/MUI Halal certification. We assist with different challenges such as stability of the active ingredient, its control release action, taste masking, shelf life, bio-availability, etc. which occur in the manufacturing process of a product with our promising micro-encapsulation solutions.</p>
                <!-- <button type="button" class="btn btn-secondary btn-sm my-3">Enquire Now</button> -->
                <button type="button" class="btn btn-secondary btn-sm my-3">Enquire Now<span>→</span></a></button>
                <div class="row mt-3">
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/1.png" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/2.png" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/3.png" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/4.png" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/5.png" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/6.png" alt="" class="img-fluid shadow">
                    </div>

                </div>
                 <div class="row mt-3 mb-3">
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/7.png" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-4">
                                            <img src="<?php echo base_url(); ?>assets/images/icons/8.png" alt="" class="img-fluid shadow">
                    </div>
                 
                    
                </div>
            </div>
            <div class="col-lg-6 col-12">
                                    <img src="<?php echo base_url(); ?>assets/images/what/1.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>


<section class="onepartner mt-5">
    <div class="container-fluid">
        <div class="row  align-items-center">
            <div class="col-lg-6 col-12 pl-5">
                <h1 class="font-weight-bold text-center text-partner pt-3 pt-lg-0">One Partner - Multiple Solutions</h1>
            
                                   <img src="<?php echo base_url(); ?>assets/images/footer logo.png" alt="" class="img-fluid m-auto d-block">
             </div>
               
            
            <div class="col-lg-6 col-12 my-5">
                                    <img src="<?php echo base_url(); ?>assets/images/what/infographics.png" alt="" class="img-fluid graphics">
            </div>
        </div>
    </div>
</section>


<section class="application11 mb-5">
    <div class="container-fluid container-custom">
        <div class="row align-items-center padd mb-3 mb-lg-0">
         <!--       <div class="col-lg-6 col-12 d-lg-none d-block">
                <h2 class="text-blue03">Your application expert</h2>
                <p class="text-black02 mt-3">We dedicate ourselves to satisfying our Clients’ requirements with innovation and honoring them with product quality, time and other commitments made to them. We have always gone beyond problem-solving to meet our customer’s expectations. We make sure our knowledge, skills, innovation and guiding principles are employed not only at the manufacturing stage but throughout the production process.</p>
                
            </div> -->
            
            <div class="col-lg-6 col-12">
                                    <img src="<?php echo base_url(); ?>assets/images/what/2.jpg" alt="" class="img-fluid what1">
            </div>
            <div class="col-lg-6 col-12 pt-lg-0 pt-3">
                <h2 class="text-blue03">Your application expert</h2>
                <p class="text-black02 mt-3">We dedicate ourselves to satisfying our Clients’ requirements with innovation and honoring them with product quality, time and other commitments made to them. We have always gone beyond problem-solving to meet our customer’s expectations. We make sure our knowledge, skills, innovation and guiding principles are employed not only at the manufacturing stage but throughout the production process.</p>
                
            </div>
        </div>
    </div>
</section>



<!-- Humberger Navbar -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/Technologies/Blending/banner.jpg"
                        class="img-fluid w-100 pb-5">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">Blending</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content1 pt-5 pb-5 ">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h5 class="color-green text-center green-pdd">In the nutraceutical and functional food industries,
                    combination products are the norm, and the most common nutrients are vitamins, minerals,
                    amino acids, nucleotides and other functional food ingredients offered in single-serving powdered
                    products, tablets, or capsule. If the compounds
                    in the formulation are not appropriately blended the</h5>
            </div>
        </div>
    </div>
</section>

<section class="mission pt-5 pb-5">
    <div class="container-fuild px-0">
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section order-xl-1 order-2">
                <div class="para-div1">
                    <p class="mech-text">Hence to achieve a correctly-proportioned blend of these active ingredients and
                        to form a uniform
                        homogenous dosage, blending is required. Blending is a process that mixes the active substance
                        with other ingredients or excipients to ensure there is a homogeneous mixture of all ingredients
                        for
                        each manufacturing process.</p>
                    <p class="mech-text">The incorporation of such nutrient blends in food fortification and enrichment
                        plays a vital role in
                        nutrient strategies such as alleviating micronutrient deficiencies or malnutrition.</p>
                </div>
            </div>
            <div class="col-md-6 px-xl-0 px-3 mb-xl-0 mb-3 col-section order-xl-2 order-1">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Blending/intro 1.jpg" alt=""
                    class="img-fluid section1">
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section img-hide">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Blending/intro 2.jpg" alt=""
                    class="img-fluid section1">
            </div>
            <div class="col-md-6 px-0 col-section order-xl-1 order-2">
                <div class="para-div1">
                    <p class="mech-text">In encapsulation technology, even though dry ingredients are mixed well before
                        encapsulation,
                        blending helps to impact on better distribution of active ingredients in the encapsulant.
                        Cohesive
                        Powders / Materials are highly attracted to each other and tend to agglomerate, reducing
                        material
                        flow. Hence blending ensures uniformity and efficient mixing during processing.</p>
                    <p class="mech-text">Dry blending is the most commonly used method to manufacture premixes. An
                        important aspect of dry blending is the physical characteristics of
                        powders including flowability, particle size, density and shape. Apart from the blending method,
                        particle size, mixing time, the speed of the
                        blender, the flow rate and volume of material to be blended are needed to be considered.</p>
                    <p class="mech-text">All such process parameters will validate the final product and care should be
                        taken throughout the blending process to maintain constant blending
                        parameters.</p>
                </div>
            </div>
            <div class="col-md-6 col-section img-show px-xl-0 px-3 mb-xl-0 mb-3  order-xl-2 order-1">
                <img src="<?php echo base_url(); ?>assets/images/advantages.jpg" alt="" class="img-fluid section1">
            </div>
        </div>
    </div>
</section>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sorbicacid.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">
</head>

<style type="text/css">

.text-header {
    font-size: 2rem;
    font-weight: bold;
}

.btn-secondary
{
    color: #a9a8a9;
    background-color: #FFFFFF;
    border-color: #F07F1B;
    border-radius: 2rem;
    text-transform: capitalize;
    padding: -0.1rem;
    padding-left: 2rem;
    padding-right: 2rem;
    box-shadow: 0px 16px 32px #0000001c;
    margin-right: 1.3rem;
}

.btn-secondary:hover
{
    color: #FFFFFF;
    background-color: #F07F1B;
    border-color: #F07F1B;
    border-radius: 2rem;
    text-transform: capitalize;
    padding: -0.1rem;
    padding-left: 2rem;
    padding-right: 2rem;
    box-shadow: 0px 16px 32px #0000001c;
    margin-right: 1.3rem;
}


@media screen  and (max-width:1279px) {

}

@media screen  and (max-width: 448px) {

}

</style>

<!-- Humberger Navbar -->


<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="<?php echo base_url(); ?>assets/images/Assets/Product detail page- Encapsulated Sorbic acid\banner.jpg"
                        class="img-fluid w-100 pb-5">
                    <!-- <img src="images/" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">Our Products</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="lis-section pb-5" id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- <div class="card">
                    <div class="card-body">
                        <!-- <ul class="encap-list">
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Sorbic Acid</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Citric Acid</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Malic Acid</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Sea Salt</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Fumaric Acid</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Ammonium Bicarbonate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Sodium Bicarbonate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Calcium Propionate</a></li>
                        </ul> --
                    </div>
                </div> -->
                <h1 class="text-header">Encapsulated Bakery Ingredients</h1>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/sobic/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Sorbic Acid </button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/critic/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Citric Acid </button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/malic/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Malic Acid </button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/sea/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Sea Salt </button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/fumaric/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Fumaric Acid</button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/Ammonium/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Ammonium Bicarbonate </button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/sodium/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Sodium Bicarbonate </button>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>products/calcium/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold">Encapsulated Calcium Propionate </button>
            </div>
        </div>
    </div>
</section>


<section class="pb-5">
<div class="container">
    <div class="row">
                <div class="col-lg-12">
                    <!-- <div class="card">
                        <h5 class="card-header">Encapsulated Bakery Ingredients</h5>
                        <div class="card-body">
                            <ul class="encap-list">
                            <li class="nav-item"><a class="nav-link" href=""> Cupric Sulfate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Ferrous Sulfate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Manganese Sulfate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Magnesium Sulfate Anhydrous</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Encapsulated Fumaric Acid</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Zinc Sulfate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Potassium Iodide</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Copper Gluconate</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Magnesium Gluconate</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Zinc Gluconate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Ferric Pyrophosphate</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Ferric Orthophosphate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Magnesium Phosphate</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Dipotassium Phosphate</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Magnesium Oxide</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Cupric Oxide</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Zinc Oxide</a></li> 
                            <li class="nav-item"><a class="nav-link" href=""> Sodium Molybdate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Sodium Selenite</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Chromium Picolinate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Calcium Citrate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Calcium Lactate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Poly Iron Complex</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Potassium Chloride</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Ferrous Fumarate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Ferrous Lactate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Reduced Iron</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Magnesium Ascorbate</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Selenomethionine</a></li>
                            <li class="nav-item"><a class="nav-link" href=""> Chromium Chloride  </a></li>
                            </ul>
                        </div>
                    </div> -->
                    <h1 class="text-header">Encapsulated Minerals</h1>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/sulfate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Cupric Sulfate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/ferrous_sulfate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Ferrous Sulfate</button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/manganese/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Manganese Sulfate</button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/magnesium_sulfate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Magnesium Sulfate Anhydrous </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/zinc_sulfate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Encapsulated Fumaric Acid</button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/potassium_iodide/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Zinc Sulfate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/copper_gluconate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Potassium Iodide </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/magnesium_gluconate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Copper Gluconate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/zinc_gluconate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Magnesium Gluconate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/ferric_pyrophosphate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Zinc Gluconate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/ferric_orthophosphate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Ferric Pyrophosphate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/magnesium_phosphate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Ferric Orthophosphate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/dipotassium_phosphate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Magnesium Phosphate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/magnesium_oxide/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Dipotassium Phosphate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/cupric_oxide/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Magnesium Oxide </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/zinc_oxide/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Cupric Oxide </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/sodium_molybdate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Zinc Oxide </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/sodium_selenite/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Sodium Molybdate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/chromium_picolinate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Chromium Picolinate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/calcium_citrate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Calcium Citrate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/calcium_lactate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Calcium Lactate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/poly_iron_complex/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Poly Iron Complex </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/potassium_chloride/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Potassium Chloride </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/ferrous_fumarate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Ferrous Fumarate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/ferrous_lactate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Ferrous Lactate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/reduced_iron/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Reduced Iron </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/magnesium_ascorbate/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Magnesium Ascorbate </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/selenomethionine/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Selenomethionine </button>
                    <button type="button" onclick="location.href='<?php echo base_url(); ?>products/chromium_chloride/product';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Chromium Chloride </button>
                </div>
            </div>
    </div>
</section>



</body>

</html>
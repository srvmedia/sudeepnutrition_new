
<style type="text/css">

/*style css starts*/

body{
    overflow-x:hidden;
}

.text-red01{
    color:#ffffff;
    font-size: 3rem;
}


.banner_text02 {
  position: absolute;
  width: 84%;
  top: 55%;
  left: 4%;
  transform: translatey(-50%);
  z-index: 999;
  bottom: 23%;
}
.text-blue03{
    color:#2C5B8D;
    font-weight: bold;
}
.text-black03{
    color: #030303;
    font-size: 1.5rem;
}

.btn-secondary2, .btn-secondary2:hover {
    color: #fff;
    background-color: #2c5b8d;
    border-color: #2c5b8d;
    border-radius: 3rem;
    text-transform: capitalize;
    padding-left: 2rem;
    padding-right: 2rem;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    box-shadow: 0px 16px 32px #0000001c;
}
.product{
    background: #F8F8F8;
    padding: 1rem 0rem;
    margin-top: 3rem;
}

.prod-text{
    font-size: 1.2rem;
    text-align: center;
}

.bor-r{
    border-right: 1px solid #e4e4e4;
}
.bor-b{
    border-bottom: 1px solid #e4e4e4;
}

.product img {
    padding-right: 0rem !important;
}

.prod-text2{
    position: relative;
    top: -60%;
    left: 0%;
    color: white;
    font-weight: bold;
    font-size: 1.75rem;
    cursor: pointer;
    text-align: center;
}
.mtprod{
    margin-top: -3.8rem;
}

.bakery-bg{
    background: linear-gradient(90deg,#2C5B8D,#103E6F);
    box-shadow: 0px 30px 35px #0000002e;
    border-radius: 0.8rem;
}

.para7{
    font-size: 1.4rem;
}

.prod-text3 {
    position: relative;
    left: -96%;
    color: white;
    font-weight: bold;
    font-size: 1.8rem;
    bottom: -14rem;
}
.close {
    position: relative;
    left: -13rem;
    top: 2rem;
    opacity: 1;
}
button.close {
    background: white;
    height: 1px;
    align-items: center;
    display: flex;
    border-radius: 50%;
    width: 1px;
    justify-content: center;
}

.tabhead{
    color: #F07F1B;
}
.modal-dialog {
        max-width: 700px !important;
}
.modal-body{
    padding: 2rem;
}

.product-img {
    width:100%;
    height:100%;
}

.why-div {
    padding: 5rem 1rem 5rem 1rem;
}

.img-bg {
    width: 20%;
}

p.text-black02 {
    font-size: 2rem;
    font-weight: 889;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #ff8021;
}

.hov:hover .overlay {
  opacity: 0.85;
}

.text {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
  font-weight: bold;
}
.hov:hover p.prod-text2 {
    margin-top: -2.8rem !important;
     transition: all 0.3s linear;
}
/*style css ends*/


/*Responsive style css starts*/
@media only screen  and (max-width: 575px){
.popular-product p {
    font-size: 1.4rem !important;
}

}

@media only screen  and (min-width: 576px) and (max-width:990px){
.popular-product p {
    font-size: 0.85rem !important;
}
.mtprod {
    margin-top: -3rem;
}
}
@media only screen  and (min-width: 991px) and (max-width:1023px){
.popular-product p {
    font-size: 1rem !important;
}
}
@media only screen  and (min-width: 1024px) and (max-width:1199px){
.popular-product p {
    font-size: 1.4rem !important;
}
}
@media only screen  and (max-width: 767px){
  .bor-rn{
    border-right: none;
}
.bor-bd{
    border-bottom: 1px solid #e4e4e4;
}
.mt1{
    margin-top: 3rem !important;
}
.banner_text02 h5{
    font-size:0.8rem;
}
}
@media only screen  and (max-width: 319px){
.banner_text02 h5{
    font-size:0.45rem;
}
}

@media only screen  and (min-width: 1025px){
.container-custom {
    padding: 0rem 7rem 0rem 7rem;
}
}

@media only screen and (max-width: 577px){
.res_cust {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
    text-align: center;
}
/*.prod-text2 {
   left: -10%;
 }*/
/* .mtprod2{
    margin-top: -3.8rem;
}*/
.reshide{
    display:none;
}

}
@media only screen and (min-width: 768px){
  .reshide{
    display:none;
}
}
@media only screen and (min-width: 576px) and (max-width: 767px){
.res_cust {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}

.mtprod3{
    margin-top: -3.8rem;
}
.reshide2{
    display:none;
}
.mtprod4{
    margin-top: 0rem;
}
}
@media only screen and (max-width: 1100px){
    .modal-dialog {
        max-width: 500px !important;
    }
    p.text-black02 {
        font-size: 1.2rem;
        font-weight: 889;
    }
}

@media screen and (min-width:1025px) and (max-width:1280px) {
    .why-div {
    padding: 2rem 1rem 2rem 1rem;
    }
    .img-bg {
    width: 25%;
    }
}

@media screen and (max-width:1024px) {
    .why-div {
    padding: 0rem 1rem 0rem 1rem;
    }
    .img-bg {
    width: 30%;
    }
}

@media screen and (max-width:999px) {
    .why-div {
    padding: 0rem 1rem 0rem 1rem;
    }
    .color-blue {
    font-size: 2rem;
    }
    .text-black03 {
    font-size: 1rem;
    }
    .prod-text {
    font-size: 1rem;
    }
    .para7 {
    font-size: 1rem;
    }
    .product {
    margin-top: 1rem;
    }   
}

@media screen and (min-width:769px) and (max-width:999px) {
    .why-div {
    padding: 0rem 1rem 0rem 1rem;
    }
    .img-bg {
    width: 30%;
    }
}

@media screen and (max-width:768px) {
    .bakery-div {
        padding: 0 4rem !important;
    }
    .text-red01 {
    font-size: 2rem;
    }
  /*  .text-white {
    font-size: 1rem;
    }*/
    .why-div {
    padding: 0 2rem;
    }
    .color-blue {
    text-align: left;
    }
    .img-bg {
    width: 38%;
    }
  /*  .prod-text2 {
    top: -35%;
    }*/
 /*   .text-white {
    font-size: 1.3rem;
    }*/
}

@media screen and (max-width: 767px) {
     .banner_text h3 {
    font-size: 1.75rem !important;
  }
}

/*Responsive style css ends*/


</style>



<!-- Humberger Navbar -->


<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/What services we provide/services we provide.jpg" class="img-fluid w-100 d-none d-lg-block">
                    <img src="<?php echo base_url(); ?>assets/images/What services we provide/m banner2.jpg" class="img-fluid w-100 d-block d-lg-none">
                    <div class="banner_text">
                        <h3 class="text-white">What services we provide</h3>
                        <div class="primary-button pt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== banner end ==========-->


<section class="microencap">
    <div class="container-fluid container-custom">
        <div class="row mt-5">
            <div class="col-lg-12">
                <p class="text-blue02 text-center">We provide a wide range of Encapsulated food ingredients with customized technological solutions for varied applications across the industries.</p>
                <p class="text-black02 text-center">For more information on our products and expertise, follow the links below:</p>
            </div>
        </div>
    </div>
</section>

<section class="popular-product mb-5">
    <div class="container px-0">
          <div class="row mt-5 no-gutters">
             <div class="col-md-4 res_cust hov">
                  <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 1.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Additives<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> -->
                  </p>
                 
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img2.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Preservatives<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
                   

              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 3.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Sweeteners<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 4.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Bakery Ingredients<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
                <div class="col-md-4 res_cust mtprod2 hov">  
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div> 
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 5.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Leavening Agents<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
                <div class="col-md-4 res_cust mtprod2 hov">
                       <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 6.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Vitamins<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 7.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Minerals<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 8.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Nutrients<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 9.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Nutraceuticals<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 101.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Amino Acids<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img11.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Probiotics<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 12.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Enzymes<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 13.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Proteins<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 14.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Botanicals<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
              <div class="col-md-4 res_cust mtprod2 hov">
                   <div class="overlay">
                        <div class="text">
                        <button type="button" class="btn btn-secondary2 btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
                        </div>
                    </div>
                  <img src="<?php echo base_url(); ?>assets/images/What services we provide/img 15.jpg" class="img-fluid product-img">
                  <p class="prod-text2 text-white mt-3">Encapsulated Essential Oils<!-- <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"> --></p>
              </div>
          </div>
       
    </div>
</section>


  <!-- The Modal -->
 <!--  <div class="modal" id="myModal">
    <div class="modal-dialog modal-dialog-centered modal-xl">
      <div class="modal-content"> -->
      
        <!-- Modal Header -->
      <!--   <div class="modal-header p-0">
          <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/popup.jpg" class="img-fluid">
          <p class="prod-text3 text-white mt-3">Encapsulated Additives </p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div> -->
        
        <!-- Modal body -->
    <!--     <div class="modal-body">
          <h4 class="font-weight-bold tabhead">Products</h4>
          <hr>
          <div class="row">
              <div class="col-md-6">
                  <h6><a href="#"><u>Sorbic Acid </u><img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                  <h6><a href="#"><u>Malic Acid</u> <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                  <h6>Fumaric Acid </h6>
                  <h6>Tartaric Acid</h6>
                  <h6>Citric Acid 50%</h6>
                  <h6>Citric Acid DC </h6>
                  <h6>Citric Acid coated with citrate</h6>
                  <h6>Citric Acid with SiO2 </h6>
                  <h6><a href="#"><u>Trisodium phosphate</u> <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                   <h6>Potassium propionate</h6>
              </div>
               <div class="col-md-6 mt-3">
                  <h6>Sodium propionate </h6>
                  <h6>Propionic acid </h6>
                  <h6>Sodium acetate Sodium & Calcium Propionate </h6>
                  <h6>Sodium & Potassium Benzoate </h6>
                  <h6>Sodium, Potassium & Calcium Sorbate</h6> 
                  <h6>Cassia Bark Extract </h6>
                  <h6>Kaffir Lime Leaf Extract</h6> 
                  <h6>Glucose Oxidase</h6>
               </div>
          </div>
        </div>
        
              
      </div>
    </div>
  </div> -->



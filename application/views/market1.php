<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


<style type="text/css">

/*style css starts*/

body{
    overflow-x:hidden;
}

.text-red01{
    color:#E31F25;
    font-size: 3rem;
}

.text-white {
    font-size: 1.5rem;
    font-weight: normal;
}

.font-bakery {
    font-size: 2.5rem;
}

.banner_text02 {
  position: absolute;
  width: 84%;
  top: 55%;
  left: 4%;
  transform: translatey(-50%);
  z-index: 999;
  bottom: 23%;
}
.text-blue03{
    color:#2C5B8D;
    font-weight: bold;
}
.text-black03{
    color: #030303;
    font-size: 1.5rem;
}
.product{
    background: #F8F8F8;
    padding: 1rem 0rem;
    margin-top: 3rem;
}

.prod-text{
    font-size: 1.2rem;
    text-align: center;
}

.bor-r{
    border-right: 1px solid #e4e4e4;
}
.bor-b{
    border-bottom: 1px solid #e4e4e4;
}

.product img {
    padding-right: 0rem !important;
}

.prod-text2{
    position: relative;
    top: -25%;
    left: 0%;
    color: white;
    font-weight: bold;
    font-size: 1.2rem;
    cursor: pointer;
    text-align: center;
}
.mtprod{
    margin-top: -3.8rem;
}

.bakery-bg{
    background: linear-gradient(90deg,#2C5B8D,#103E6F);
    box-shadow: 0px 30px 35px #0000002e;
    border-radius: 0.8rem;
}

.para7{
    font-size: 1.4rem;
}

.prod-text3 {
    position: relative;
    left: -96%;
    color: white;
    font-weight: bold;
    font-size: 1.8rem;
    bottom: -14rem;
}
.close {
    position: relative;
    left: -13rem;
    top: 2rem;
    opacity: 1;
}
button.close {
    background: white;
    height: 1px;
    align-items: center;
    display: flex;
    border-radius: 50%;
    width: 1px;
    justify-content: center;
}

.tabhead{
    color: #F07F1B;
}
.modal-dialog {
        max-width: 700px !important;
}
.modal-body{
    padding: 2rem;
}

.product-img {
    width:100%;
    height:100%;
}

.res_cust {
    flex: 0 0 20%;
    max-width: 20%;
}

.why-div {
    padding: 5rem 1rem 5rem 1rem;
}

.img-bg {
    width: 20%;
}
/*style css ends*/


/*Responsive style css starts*/

@media only screen  and (min-width: 768px) and (max-width:990px){
.popular-product p {
    font-size: 0.65rem;
}
.mtprod {
    margin-top: -3rem;
}
}
@media only screen  and (min-width: 991px) and (max-width:1279px){
.popular-product p {
    font-size: 0.85rem;
}
}
@media only screen  and (max-width: 767px){
  .bor-rn{
    border-right: none;
}
.bor-bd{
    border-bottom: 1px solid #e4e4e4;
}
.mt1{
    margin-top: 3rem !important;
}
.banner_text02 h5{
    font-size:0.8rem;
}
}
@media only screen  and (max-width: 319px){
.banner_text02 h5{
    font-size:0.45rem;
}
}

@media only screen  and (min-width: 1025px){
.container-custom {
    padding: 0rem 7rem 0rem 7rem;
}
}

@media only screen and (max-width: 577px){
.res_cust {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
    text-align: center;
}
.prod-text2 {
   left: -10%;
 }
 .mtprod2{
    margin-top: -3.8rem;
}
.reshide{
    display:none;
}

}
@media only screen and (min-width: 768px){
  .reshide{
    display:none;
}
}
@media only screen and (min-width: 576px) and (max-width: 767px){
.res_cust {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}

.mtprod3{
    margin-top: -3.8rem;
}
.reshide2{
    display:none;
}
.mtprod4{
    margin-top: 0rem;
}
}
@media only screen and (max-width: 1100px){
    .modal-dialog {
        max-width: 500px !important;
    }
}

@media screen and (min-width:1025px) and (max-width:1280px) {
    .why-div {
    padding: 2rem 1rem 2rem 1rem;
    }
    .img-bg {
    width: 25%;
    }
    .challenge{
      margin-left: 3rem;
      margin-right: 3rem;
    }
}

@media screen and (max-width:1024px) {
    .why-div {
    padding: 0rem 1rem 0rem 1rem;
    }
    .img-bg {
    width: 30%;
    }
}

@media screen and (max-width:999px) {
    .why-div {
    padding: 0rem 1rem 0rem 1rem;
    }
    .color-blue {
    font-size: 2rem;
    }
    .text-black03 {
    font-size: 1rem;
    }
    .prod-text {
    font-size: 1rem;
    }
    .para7 {
    font-size: 1rem;
    }
    .product {
    margin-top: 1rem;
    }   
}

@media screen and (min-width:769px) and (max-width:999px) {
    .why-div {
    padding: 0rem 1rem 0rem 1rem;
    }
    .img-bg {
    width: 30%;
    }
}

@media screen and (max-width:768px) {
    .bakery-div {
        padding: 0 4rem !important;
    }
    .text-red01 {
    font-size: 2rem;
    }
    .text-white {
    font-size: 1rem;
    }
    .why-div {
    padding: 0 2rem;
    }
    .color-blue {
    text-align: left;
    }
    .img-bg {
    width: 38%;
    }
    .prod-text2 {
    top: -35%;
    }
    .text-white {
    font-size: 1.3rem;
    margin-top: 3rem;
    }
    .product_top{
      padding: 1rem;
    }
}

@media (min-width: 1440px) and (max-width: 1920px) {
  .challenge {
    margin-left: 11rem;
    margin-right: 11rem;
  }
}

.challenge1{
  padding-bottom: 4rem;

}

/*Responsive style css ends*/

</style>

</head>

<!-- Humberger Navbar -->


<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
  
                    <img src="<?php echo base_url(); ?><?php echo $main_header[0]->image; ?>" class="img-fluid w-100 d-none d-lg-block">
                    <img src="<?php echo base_url(); ?><?php echo $main_header[0]->mobile_image; ?>"  class="img-fluid w-100 d-block d-lg-none">
                                       
                        <div class="banner_text">
                        <h1 class="text-red01 font-weight-bold"><?php echo $main_header[0]->sub_section_header; ?></h1>
                        <h5 class="text-white mb-3"><?php echo $content[0]->header; ?>
                        </h5>
                        </div>
</section>
<!--====== banner end ==========-->


<section class="whysudeep">
    <div class="container px-0">
        <div class="row mt-5">
            <div class="col-lg-7 col-12 why-div">
                <h1 class="color-blue">Why Sudeep Nutrition?</h1>
                <p class="text-black03"><?php echo $content[0]->sub_header; ?></p>
                <button type="button" onclick="location.href='<?php echo base_url(); ?>compliance';" class="btn btn-secondary btn-sm my-3 font-weight-bold"> Enquire Now  <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"></button>
            </div>
            <div class="col-lg-5 col-12">
            <img src="<?php echo base_url(); ?><?php echo $content[0]->image; ?>" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>

<?php if($sub_tab==1 || $sub_tab==2 || $sub_tab==3 || $sub_tab==4 || $sub_tab==5 || $sub_tab==6) : ?>
<section class="product">
    <div class="container-fluid container-custom px-0">
          <h1 class="color-blue text-center challenge1">We help you overcome everyday product<br> challenges like</h1>
          <div class="row mt-5 no-gutters challenge">
          <?php foreach ($challenges as $challenge) { ?>
            <div class="col-md-3 col-6 bor-r bor-b mt-4">
                  <img src="<?php echo base_url(); ?><?php echo $challenge->image; ?>" class="img-fluid d-block m-auto img-bg">
                  <p class="prod-text mt-3"><?php echo $challenge->challenges; ?>
              </div>

          <?php } ?>
          </div>
           <!-- <div class="row mb-5 no-gutters">

          </div> -->
    </div>
</section>
<?php endif; ?>


<section class="popular-product mb-5">
    <div class="container-fluid px-0">
          <h1 class="color-blue text-center mt-5">Our Most Popular Products</h1>
          <div class="row mt-5 no-gutters product_top" style="
                                                        display: flex;
                                                        justify-content: center;">
          <?php foreach ($products as $product) { ?>
          <div class="col-md-2 res_cust mtprod2">
                <img src="<?php echo base_url(); ?><?php echo $product->image; ?>" class="img-fluid product-img">
                <p class="prod-text2 text-white mt-3" data-toggle="modal" data-target="#myModal<?php echo $product->id; ?>"><?php echo $product->label; ?> <img src="<?php echo base_url(); ?><?php echo $product->arrow_image; ?>" class="img-fluid pl-2"></p>
          </div>
          <?php } ?>
        </div>
    </div>
</section>


<section class="bakery mb-5">
    <div class="container px-0">
          <div class="row bakery-bg align-items-center no-gutters">
            <div class="col-lg-1"></div>
             <div class="col-lg-7 bakery-div">
                 <h1 class="text-white font-weight-bold font-bakery">Encapsulated Bakery Ingredients Annexure</h1>
                 <p class="text-white mt-3 para7">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do eiusmod tempor erto incididunt ut labore et dolore magna gusta consectetur eyter.</p>
                 <button type="button" class="btn btn-secondary btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
             </div>
            <div class="col-lg-3">
            <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/img2.png" class="img-fluid" style="width:80%">
            </div>
            <div class="col-lg-1"></div>
          </div>
           
    </div>
</section>






  <!-- The Modal -->
 



</body>

</html>
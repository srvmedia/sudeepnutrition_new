<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/sorbicacid.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">


</head>



<!-- Humberger Navbar -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Granulation/banner.jpg">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/Assets/" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">Granulation</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content1 pt-5 pb-5 ">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h5 class="color-green green-pdd text-center">Granulation is a widely used essential process technology, in which
                    primary powder particles adhere to each other, resulting in larger homogenous multiparticle entities
                    or granules.

                    It enhances the density of a drug/active substance and is widely used as an intermediate process
                    within solid dosage manufacturing.

                    Material densification of powders and increase in the particle size is ensured for a better flow of
                    distributed material which is an important factor in the production of tablets and capsules using
                    high speed manufacturing equipment.</h5>
            </div>
        </div>
    </div>
</section>

<section class="mission pt-5 pb-5">
    <div class="container-fuild px-0">
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section">
                <div class="para-div1">
                    <h5 class="color-blue padd-p">Dry Granulation</h5>
                    <p class="mech-text">This method involves two pieces of equipment, a compressing machine for the dry
                    powders to convert into compacts or flakes, and a miller for breaking up these intermediate products
                    into granules. The dry method is useful for ingredients that do not compress well after wet
                    granulation, or those which are sensitive to moisture.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Granulation/intro 1.jpg"  alt="" class="img-fluid section1">
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section img-hide">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Granulation/intro 2.jpg" alt="" class="img-fluid section1">
            </div>
            <div class="col-md-6 px-0 col-section">
                <div class="para-div1">
                <h5 class="color-blue padd-p">Wet Granulation</h5>
                <p class="mech-text">In this method, the wet mass is passed through a sieve to produce wet granules which
                    are then dried. It is often, used as an alternative to dry granulation, or when a rapid drying time
                    is required. The screening process breaks agglomerates into granules. Organic solvents are utilized
                    for processing when water-sensitive drugs are processed.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section img-show">
                <img src="<?php echo base_url(); ?>assets/images/aboutus/our mission.jpg" alt="" class="img-fluid section1">
            </div>
        </div>
    </div>
</section>



</body>

</html>
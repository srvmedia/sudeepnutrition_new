

    <style type="text/css">

    .text {
        font-size: 1.375rem
    }
    .text1 {
    color: #585858;
    font-size: 1.125rem;
    }
    .btn {
    border: 2px solid #000000;
    }
    button.btn.btn-outline-dark {
    border-radius: 3rem !important;
    }   

/*responsive part starts*/
@media screen and (max-width: 767px) {
     .banner_text h3 {
    font-size: 1.75rem !important;
  }
}
/*responsive part ends*/
    </style>



<!-- Humberger Navbar -->




<!-- Banner -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/what/Join our team.jpg" class="img-fluid w-100 d-none d-lg-block">
                     <img src="<?php echo base_url(); ?>assets/images/what/m_banner.jpg" class="img-fluid w-100 d-block d-lg-none">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/aboutus/banner-1.jpg" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <h3 class="text-white">Join Our Team</h3>
                        <div class="primary-button pt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Banner -->

<div class="pt-2 hiring px-0 mb-5">
    <div class="container">
       <!--  <h3 class="pt-5 pb-5 text-center head1">We Are Hiring</h3> -->
        <!-- <span class="strong-text">Hiring</span> -->
        <div class="col-md-12 pt-5">
            <div class="row no-gutters">
                <div class="col-md-6 text-left ">
                    <div class="text"><strong>Product designer</strong></div>
                    <p class="text1">2 Years Relevant Experience</p>
                </div>
                <div class="col-md-6 ">
                    <button type="button" class="btn btn-outline-dark">Apply</button>
                </div>
            </div>
        </div>


        <hr>

        <div class="col-md-12 ">
            <div class="row no-gutters">
                <div class="col-md-6 text-left ">
                    <div class="text"><strong>Account Director</strong></div>
                    <p class="text1">2 Years Relevant Experience</p>
                </div>
                <div class="col-md-6 ">

                    <button type="button" class="btn btn-outline-dark">Apply</button>


                </div>
            </div>
        </div>

        <hr>

        <div class="col-md-12 ">
            <div class="row no-gutters">
                <div class="col-md-6 text-left ">
                    <div class="text"><strong>DMP Data Engineer</strong></div>
                    <p class="text1">2 Years Relevant Experience</p>
                </div>
                <div class="col-md-6 ">

                    <button type="button" class="btn btn-outline-dark">Apply</button>


                </div>
            </div>
        </div>

        <hr>

        <div class="col-md-12 ">
            <div class="row no-gutters">
                <div class="col-md-6 text-left ">
                    <div class="text"><strong>Account Manager</strong></div>
                    <p class="text1">2 Years Relevant Experience</p>
                </div>
                <div class="col-md-6 ">

                    <button type="button" class="btn btn-outline-dark">Apply</button>


                </div>
            </div>
        </div>

        <hr>

        <div class="col-md-12 ">
            <div class="row no-gutters">
                <div class="col-md-6 text-left ">
                    <div class="text"><strong>Market Director</strong></div>
                    <p class="text1">2 Years Relevant Experience</p>
                </div>
                <div class="col-md-6 ">

                    <button type="button" class="btn btn-outline-dark">Apply</button>


                </div>
            </div>
        </div>

        <hr>

        <div class="col-md-12 ">
            <div class="row no-gutters">
                <div class="col-md-6 text-left ">
                    <div class="text"><strong>Staff Accountant</strong></div>
                    <p class="text1">2 Years Relevant Experience</p>
                </div>
                <div class="col-md-6 ">

                    <button type="button" class="btn btn-outline-dark">Apply</button>


                </div>
            </div>
        </div>

        <hr>
    </div>
</div>

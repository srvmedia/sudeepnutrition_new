

<style type="text/css">

/*style css starts*/


.shadow {
    width: 100%;
    height: 100%
}

img.img-fluid.shadow {
   box-shadow: 0px 5px 20px #0000001a !important
    border-radius: 5px;
}

body{
    overflow-x:hidden;
}

.btn-secondary, .btn-secondary:hover {
    color: #fff;
    background-color: #F07F1B;
    border-color: #F07F1B;
    border-radius: 3rem;
    text-transform: capitalize;
    padding-left: 2rem;
    padding-right: 2rem;
    padding-top: 0.7rem;
    padding-bottom: 0.7rem;
    box-shadow: 0px 16px 32px #0000001c;
}

p.text-blue02 {
    font-size: 1.8rem;
}

@media (max-width: 1200px) {
    p.text-blue02 {
    font-size: 1.2rem;
    }
    .mt-5 {
    margin-top: 1rem!important;
    }
    .hide {
        display:none
    }
}
/*Responsive style css ends*/

</style>



<!-- Humberger Navbar -->

<!--====== banner start ==========-->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/banner.jpg"
                        class="img-fluid w-100 pb-5">
                        <!-- <img src="<?php echo base_url(); ?>assets/images/Accrediations & Certification/banner1.jpg" class="img-fluid w-100 d-none d-lg-block pb-5"> -->
                      <!-- <img src="<?php echo base_url(); ?>assets/images/Accrediations & Certification/banner1m.jpg" class="img-fluid w-100 d-block d-lg-none pb-5"> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== banner end ==========-->


<section class="microencap">
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 col-12">
                <p class="text-blue02">Our operations are certified to the highest standards worldwide. FSSC 22000 is one of the highest foods and safety standards developed for food manufacturers. We also apply all relevant Good Manufacturing Practice (GMP) Guidelines and standards. </p>
                <p class="text-blue02">We have ISO 9001, ISO 14001, GMP, HALAL and KOSHER certificates beside this.</p>
                <!-- <button type="button" class="btn btn-secondary btn-sm my-3">Enquire Now</button> -->
                <button type="button" class="btn btn-secondary btn-sm my-3">Enquire Now<span>→</span></a></button>
            </div>
            <div class="col-lg-6 col-12">
                <div class="row mt-3">
                    <div class="col-lg-2 col-md-2 col-sm-4 hide">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/FSSAI.jpg" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-md-2 col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/FSSC.jpg" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/GMP.jpg" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/HACCP.jpg" alt="" class="img-fluid shadow">
                    </div>
                </div>
                <div class="row mt-3 mb-5">
                    <div class="col-lg-2 col-md-2  col-sm-4 hide">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/HALAL.jpg" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/ISO 1.jpg" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/ISO 2.jpg" alt="" class="img-fluid shadow">
                    </div>
                    <div class="col-lg-2 col-md-2  col-3">
                        <img src="<?php echo base_url(); ?>/assets/images/Accrediations & Certification/Kosher.jpg" alt="" class="img-fluid shadow">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

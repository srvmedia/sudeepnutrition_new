<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sorbicacid.css">

</head>

<style type="text/css">

.eco-div {
    padding: 3rem!important;
}
.card {
    height: 100%;
    position: relative;
    border: 1px solid rgba(0, 0, 0, 0);
    overflow: hidden;
    border-radius: 0.7rem;
}

.card-body {
    padding: 2rem 0px;
}

.card:hover {
    transform: none;
    box-shadow: none;
}

.card-title {
    font-size: 1.5rem;
    font-weight: bold;
}
p.card-description {
    color: #484848;
}
.mission-height {
    height:90%;
}

.clr {
    font-size: 1.8rem;
    color:#2C5B8D;
    font-weight: bold;
}

.font1 {
    font-size: 1.25rem;
    font-family: Raleway;
}

.long-text {
	display: none;
    font-weight: normal;
    font-size: 1rem;
}

.show-more-button {
    color: #000;
    font-weight: bold;
    cursor: pointer;
    font-size: 1rem;
}

@media screen and (max-width:1024px) {
    .padd-p {
    padding: 0;
    }
    .clr {
        font-size: 1.3rem !important;
    }
}

@media screen and (min-width: 768px) and (max-width: 999px)
{
    .font1 {
    font-size: 1rem;
    }
}

@media screen and (max-width: 768px)
{
    .eco-div {
    padding: 1rem!important;
    }
    .color-white {
    font-size: 2rem;
    }
    .bt_pd {
    padding-bottom: 1rem;
    }
    .leaders {
    padding: 2rem 0;
    }
}

@media screen and (max-width: 767px)
{
    .mission-height {
    height: 100%;
    }
    .three-div {
        justify-content: center;
    }
    .div3 {
        padding-top: 0;
        padding-bottom: 0;
    }
}

</style>

<!-- Humberger Navbar -->

<!-- Banner -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/aboutus/banner.jpg"
                        class="img-fluid w-100 pb-5 d-none d-lg-block">
                    <img src="<?php echo base_url(); ?>assets/images/banner_mb_aboutus.jpg"
                        class="img-fluid w-100 pb-5 d-block d-lg-none">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">About Us</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Banner -->

<!-- Content section start -->

<section class="content pt-5 pb-5 ">
    <div class="container px-0">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h5 class="color-blue-pra clr">Sudeep Nutrition Pvt. Ltd is specialized in the manufacture of Encapsulated
                    ingredient solutions
                    which focuses on solving customer application problems using a wide range of Microencapsulation and
                    Particle coating technologies.</h5>
            </div>
            <div class="col-sm-12 col-md-6">
                <h5 class="color-grey1 font1">At Sudeep Nutrition all our services are based on customized solutions
                    that
                    facilitate the use and
                    optimize the performance of ingredients, supplements and active compounds, via microencapsulation.
                    We deliver our technical expertise over a broad spectrum of industries such as Food, Nutrition and
                    Pharmaceuticals.</h5>
            </div>
        </div>
    </div>
</section>

<!-- Content section End -->

<!-- Our Mission -->

<!-- <section class="mission">
    <div class="container px-0">
        <div class="row">
            <div class="col-lg-6 mission-div">
                <h1 class="color-blue">Our Mission</h1>
                <div class="col-lg-6 px-0 mission-img  d-lg-none d-block">
                    <img src="<?php echo base_url(); ?>assets/images/aboutus/our mission.jpg" class="miss-img">
                </div>
                <p class="p-text">We strive to deliver services based on high scientific standards and advanced
                    technical skills in the area of Microencapsulation, Granulation, Micronizing, and Spray drying.</p>
                <p class="p-text"> We believe our core values will help customers meet development challenges and with
                    our depth of knowledge, experience, regulatory know-how and technical expertise, we ensure success
                    in any process development phase with our micro- encapsulation services.</p>
                <div class="row">
                    <div class="col-md-4">
                        <div class="div2">
                            <img src="<?php echo base_url(); ?>assets/images/aboutus/mission 1.svg" alt="" class="img-fluid">
                            <p class="font2">Sustainable and Reliable solutions with microencapsulation (business
                                approach)</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="div3">
                            <img src="<?php echo base_url(); ?>assets/images/aboutus/mission 2.svg" alt="" class="img-fluid">
                            <p class="font2">Innovation, Integrity, Excellence (our passion).</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="div4">
                            <img src="<?php echo base_url(); ?>assets/images/aboutus/mission 3.svg" alt="" class="img-fluid">
                            <p class="font2">Efficient Solutions for Better Growth (our responsibility)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 px-0 mission-img d-lg-block d-none  ">
                <img src="<?php echo base_url(); ?>assets/images/aboutus/our mission.jpg" class="miss-img">
            </div>
        </div>
    </div>
</section> -->

<!-- End of Mission -->

<!-- Innovation Start-->
<!-- <section class="mission">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 px-0 d-lg-block d-none">
                <img src="<?php echo base_url(); ?>assets/images/aboutus/innovation.jpg" alt="" class="img-fluid">
            </div>
            <!-- innov-div --
            <div class="col-md-6 top_pd">
                <h1 class="color-blue ">Innovation</h1>
                <p class="p-text">Our Innovative approaches are led by the interactions between lateral thinkers and
                    experts, between
                    those on the inside and those on the outside. We employ rapidity and interaction with customers,
                    science and trends which drive the industry, market and associations ,which form the basis for the
                    culture of innovation and ideas for efficient processing. </p>
                <p class="p-text">Our Vision and Code of conduct underpinned
                    by leading-edge science serves as a framework to meet development challenges faced by the customers
                    and assure promising results in their business. </p>
                <p class="p-text">We are motivated by working out on challenges and
                    work with your R&D team to create innovative solutions that help to minimize risks and open the door
                    of possibilities.</p>

            </div>
            <div class="col-md-6 px-0  d-lg-none d-block">
                <img src="<?php echo base_url(); ?>assets/images/aboutus/innovation.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section> -->

<!-- Innovation End -->

<section class="mission pt-5 pb-5">
    <div class="container-fuild px-0">
        <div class="row no-gutters">
            <div class="col-md-6 order-xl-1 order-2 px-0 col-section">
                <div class="para-div1">
                    <h5 class="color-blue pt-lg-0 pt-4">Our Mission</h5>
                    <p class="mech-text">We strive to deliver services based on high scientific standards and advanced
                    technical skills in the area of Microencapsulation, Granulation, Micronizing, and Spray drying.</p>
                    <p class="mech-text"> We believe our core values will help customers meet development challenges and with
                    our depth of knowledge, experience, regulatory know-how and technical expertise, we ensure success
                    in any process development phase with our micro- encapsulation services.</p>
                    <div class="row three-div">
                    <div class="col-6 col-md-4 pb-lg-0 pb-4">
                        <div class="div2 mission-height">
                            <img src="<?php echo base_url(); ?>assets/images/aboutus/mission 1.svg" alt="" class="img-fluid">
                            <p class="font2">Sustainable and Reliable solutions with microencapsulation (business
                                approach)</p>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 pb-lg-0 pb-4">
                        <div class="div3 mission-height">
                            <img src="<?php echo base_url(); ?>assets/images/aboutus/mission 2.svg" alt="" class="img-fluid">
                            <p class="font2">Innovation, Integrity, Excellence (our passion).</p>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 pb-lg-0 pb-4">
                        <div class="div4 mission-height">
                            <img src="<?php echo base_url(); ?>assets/images/aboutus/mission 3.svg" alt="" class="img-fluid">
                            <p class="font2">Efficient Solutions for Better Growth (our responsibility)</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-6 order-xl-2 order-1 px-0 col-section">
                <img src="<?php echo base_url(); ?>assets/images/aboutus/our mission.jpg"  alt="" class="img-fluid section1 miss-img">
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 order-xl-1 order-1 px-0 order-md-2 order-1 col-section">
                <img  src="images/aboutus/innovation.jpg"  alt="" class="img-fluid section1">
            </div>
            <div class="col-lg-6 order-xl-2 order-2 px-0 col-section">
                <div class="para-div1 inno-padd">
                <h5 class="color-blue padd-p pt-lg-0 pt-4">Innovation</h5>
                <p class="mech-text">Our Innovative approaches are led by the interactions between lateral thinkers and
                    experts, between
                    those on the inside and those on the outside. We employ rapidity and interaction with customers,
                    science and trends which drive the industry, market and associations ,which form the basis for the
                    culture of innovation and ideas for efficient processing.</p>
                    <p class="mech-text">Our Vision and Code of conduct underpinned
                    by leading-edge science serves as a framework to meet development challenges faced by the customers
                    and assure promising results in their business.</p>
                    <p class="mech-text">We are motivated by working out on challenges and
                    work with your R&D team to create innovative solutions that help to minimize risks and open the door
                    of possibilities.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CSR Activities -->

<section class="csr">
    <div class="container">
        <div class="row">
            <div class="col-md-12  pt-5  csr-div">
                <h5 class="color-blue text-center">CSR Activities</h5>
                <p class="text-center p-text">At Sudeep Nutrition, not only our customers are part of our
                    responsibility but we
                    also extend our responsibility towards the nation, community and environment at large.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 eco-div">
                <!-- <img src="<?php echo base_url(); ?>assets/images/aboutus/csr 1.png" alt="" class="img-fluid">
                <h6 class="padd">Eco- Friendly</h6>
                <p class="csr1">As long as we are committed to the development of our business, we
                    assure to be committed towards the environment by decreasing the carbon footprints and take measures
                    to ensure the reasonable use of resources.</p> -->
                <div class="card eco-card">
                    <div class="card-img">
                    <img class="img-fluid card-img-top" src="<?php echo base_url(); ?>assets/images/eco- friendly.png"  alt="Course image">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">Eco- Friendly</h4>
                    <p class="card-description short-text">As long as we are committed to the development of our business, we
                    assure to be committed towards the environment by decreasing the carbon footprints and take measures
                    <span class="long-text">
                    to ensure the reasonable use of resources.</span>
                    <span class="text-dots">....</span>
	                <span class="show-more-button" data-more="0">Read More</span>
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 eco-div">
                <!-- <img src="<?php echo base_url(); ?>assets/images/aboutus/csr 2.png" alt="" class="img-fluid">
                <h6 class="padd">Supply Chain</h6>
                <p class="csr1">We believe in excellence whilst ensuring a strong Code of conduct
                    and integrity in our business. Thus, we rely on the quality and standards of our suppliers. Hence,
                    we believe that it is our responsibility to ensure all protocols are met at every step, that we and
                    our customers expect.</p> -->
                <div class="card supply-card">
                    <div class="card-img">
                    <img class="img-fluid card-img-top" src="<?php echo base_url(); ?>assets/images/supplu chain.png"  alt="Course image">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">Supply Chain</h4>
                        <p class="card-description short-text">We believe in excellence whilst ensuring a strong Code of conduct
                    and integrity in our business. Thus, we rely on the quality and standards of our suppliers. Hence,
                    <span class="long-text">we believe that it is our responsibility to ensure all protocols are met at every step, that we and
                    our customers expect.</span>
                    <span class="text-dots">....</span>
	                <span class="show-more-button" data-more="0">Read More</span>
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 eco-div">
                <!-- <img src="<?php echo base_url(); ?>assets/images/aboutus/csr 3.png" alt="" class="img-fluid">
                <h6 class="padd">Giving Back</h6>
                <p class="csr1">We recognize our responsibility towards the community and have been
                    working continuously for the enhancement of deprived sections of society. With diverse CSR projects,
                    Sudeep Nutrition reaches out to the underprivileged and needy to touch their lives by bestowing
                    opportunities for learning and education.</p> -->
                <div class="card supply-card">
                    <div class="card-img">
                    <img class="img-fluid card-img-top" src="<?php echo base_url(); ?>assets/images/CSR.png"  alt="Course image">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">Giving Back</h4>
                        <p class="card-description short-text">We recognize our responsibility towards the community and have been
                    working continuously for the enhancement of deprived sections of society. With diverse CSR projects,
                    <span class="long-text">Sudeep Nutrition reaches out to the underprivileged and needy to touch their lives by bestowing
                    opportunities for learning and education.</span>
                    <span class="text-dots">....</span>
	                <span class="show-more-button" data-more="0">Read More</span>
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- End CSR Activities -->


<!-- Leaders Start -->
<!-- pt-5 pb-5 -->
<section class="leaders mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="color-white bt_pd">Meet Our Leaders</h1>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="picture">
                    <img src="<?php echo base_url(); ?>assets/images/Sujit.jpg" alt="" class="abt_us_img img-fluid l1">
                    <h3 class="people">Sujit Bhayani</h3>
                    <h5 class="ceo-text">Director</h5>
                    <p class="bod short-text">Mr Sujit Bhayani, our Director has been a vivid Leader. He is endowed
                        with a financial acumen, has 21 years of experience in leading a well-established Company. 
                        He is the Managing Director at Sudeep pharma Pvt Ltd and with
                        <span class="long-text"> his experience he offers professional
                        expertise in establishing policies that promote company culture and vision; with his intentions
                        to make this company the first of its kind.</span>
                        <span class="text-dots">....</span>
	                    <span class="show-more-button text-white" data-more="0">Read More</span></p>
                    <div class="row justify-content-left mt-3">
                        <img src="<?php echo base_url(); ?>assets/images/social media-2.svg" alt="" class="img-fluid">
                        <img src="<?php echo base_url(); ?>assets/images/social media-1.svg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="picture">
                    <img src="<?php echo base_url(); ?>assets/images/Sahil.jpg" alt="" class=" abt_us_img img-fluid l1">
                    <h3 class="people">Shanil Bhayani</h3>
                    <h5 class="ceo-text">Director</h5>
                    <p class="bod short-text">Mr Shanil Bhayani, our Director, is a graduate in Business
                        Administration, Operation Management and Finance from Drexel University. He is the young link
                        who comes from a Business Background and hence understands and reads the need of the business at
                        <span class="long-text"> vast level. He plans and registers each and every aspect of the business,may it be the smallest
                        or the largest prospect and thus projects to serve the users of our products to the best of our
                        potential.</span>
                        <span class="text-dots">....</span>
	                    <span class="show-more-button text-white" data-more="0">Read More</span>
                    </p>
                    <div class="row justify-content-left mt-3">
                        <img src="<?php echo base_url(); ?>assets/images/social media-2.svg" alt="" class="img-fluid">
                        <img src="<?php echo base_url(); ?>assets/images/social media-1.svg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="picture">
                    <img src="<?php echo base_url(); ?>assets/images/Nils.jpg" alt="" class=" abt_us_img img-fluid l1">
                    <h3 class="people">Nils Gersonde</h3>
                    <h5 class="ceo-text">Director</h5>
                    <p class="bod short-text">Mr Nils Gersonde, our Director, is highly experienced in the field of
                        operational management and corporate development of international affiliated companies. He is a
                        Business Graduate from WHU Otto Beisheim Graduate School of Management. With tremendous 
                        <span class="long-text">amount
                        of experience behind him, all our related businesses are in safe hands, as we see the best of
                        the inputs coming from him.</span>
                        <span class="text-dots">....</span>
	                    <span class="show-more-button text-white" data-more="0">Read More</span>
                        </p>
                    <div class="row justify-content-left mt-3">
                        <img src="<?php echo base_url(); ?>assets/images/social media-2.svg" alt="" class="img-fluid">
                        <img src="<?php echo base_url(); ?>assets/images/social media-1.svg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="picture">
                    <img src="<?php echo base_url(); ?>assets/images/Hempe.jpg" alt="" class=" abt_us_img img-fluid l1">
                    <h3 class="people">Nils Gersonde</h3>
                    <h5 class="ceo-text">Director</h5>
                    <p class="bod short-text">Mr Nils Gersonde, our Director, is highly experienced in the field of
                        operational management and corporate development of international affiliated companies. He is a
                        Business Graduate from WHU Otto Beisheim Graduate School of Management. 
                        <span class="long-text">With tremendous amount
                        of experience behind him, all our related businesses are in safe hands, as we see the best of
                        the inputs coming from him.</span>
                        <span class="text-dots">....</span>
	                    <span class="show-more-button text-white" data-more="0">Read More</span></p>
                    <div class="row justify-content-left mt-3">
                        <img src="<?php echo base_url(); ?>assets/images/social media-2.svg" alt="" class="img-fluid">
                        <img src="<?php echo base_url(); ?>assets/images/social media-1.svg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Leaders End -->




<script>
    $('span').on('click', function() {
	// If text is shown less, then show complete
	if($(this).attr('data-more') == 0) {
		$(this).attr('data-more', 1);
		$(this).css('display', 'inline');
		$(this).text('Read Less');

		$(this).prev().css('display', 'none');
		$(this).prev().prev().css('display', 'inline');
	}
	// If text is shown complete, then show less
	else if(this.getAttribute('data-more') == 1) {
		$(this).attr('data-more', 0);
		$(this).css('display', 'inline');
		$(this).text('Read More');

		$(this).prev().css('display', 'inline');
		$(this).prev().prev().css('display', 'none');
	}
});
</script>

</body>

</html>
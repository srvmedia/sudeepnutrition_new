
<style type="text/css">

/*style css starts*/


.btn-secondary, .btn-secondary:hover {
    color: #fff;
    background-color: #F07F1B;
    border-color: #F07F1B;
    border-radius: 3rem;
    text-transform: capitalize;
    padding-left: 2rem;
    padding-right: 2rem;
    padding-top: 0.7rem;
    padding-bottom: 0.7rem;
    box-shadow: 0px 16px 32px #0000001c;
}

.why-div {
    margin: auto; 
}

p.text-blue02 {
    font-size: 1.8rem;
}
/*style css ends*/



/*responsive part starts*/
@media (max-width: 1200px) {
    p.text-blue02 {
    font-size: 1.2rem;
    }
    .why-div {
        padding: 0 1rem !important;
    }
}

@media screen and (max-width: 767px) {
     .banner_text h3 {
    font-size: 1.75rem !important;
  }
}
/*responsive part ends*/

</style>



<!-- Humberger Navbar -->

<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
    <img src="<?php echo base_url(); ?>/assets/images/Contract Manufacturing/banner.jpg" class="img-fluid w-100">
    <!-- <img src="<?php echo base_url(); ?>assets/images/Contract Manufacturing/banner2.jpg" class="img-fluid w-100 d-none d-lg-block"> -->
    <!-- <img src="<?php echo base_url(); ?>assets/images/Contract Manufacturing/banner2m.jpg" class="img-fluid w-100 d-block d-lg-none"> -->
</section>
<!--====== banner end ==========-->


<section class="whysudeep">
    <div class="container px-0">
        <div class="row no-gutters mt-5">
             <div class="col-lg-6 col-sm-12 text-right mb-5 d-block d-lg-none">
                <img src="<?php echo base_url(); ?>/assets/images/Contract Manufacturing/intro1.jpg" alt="" class="img-fluid">
            </div>
            <div class="col-lg-6 col-sm-12 why-div">
                <p class="text-blue02">In addition to our full-scale manufacturing services provided to our customers, Sudeep Nutrition also offers Contract Manufacturing services to customers interested in different product manufacturing services other than encapsulation. </p>
                <p class="text-blue02"> With our entrepreneurial approach and expertise in advanced technology, we ensure customer’s reliability on the solutions and services provided through our expert team.</p>
                <button type="button" class="btn btn-secondary btn-sm my-3 font-weight-bold">Enquire Now  <img src="<?php echo base_url(); ?>/assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"></button>
            </div>
            <div class="col-lg-6 col-sm-12 text-right mb-5 d-lg-block d-none">
                <img src="<?php echo base_url(); ?>/assets/images/Contract Manufacturing/intro1.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>


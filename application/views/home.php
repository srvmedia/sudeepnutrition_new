<!-- 

1) Blending
2) contact us
3) Home page
4) Product Information
5) spray

 -->


<style type="text/css">
.img-bg {
    background: linear-gradient(90deg, #E31F25, #FF1F26);
    border-radius: 50%;
    width: 35%;
}
#market-carousel .owl-wrapper-outer {
    /* display: flex; */
    overflow: hidden;
}

#market-carousel .owl-wrapper-outer .owl-item {
    margin-right: 12px;
}
</style>



<!-- Humberger Navbar -->

<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/banner.jpg"
                        class="img-fluid w-100 d-md-block d-none">
                    <img src="<?php echo base_url(); ?>assets/images/mbanner.jpg"
                        class="img-fluid w-100 d-block d-md-none">
                    <div class="banner_text">
                        <h3><span class="text_white">Delivering </span><span class="text_red">better
                                nutrition</span>
                            <span class="text_white">for <br>every step of life’s journey</span>
                        </h3>
                        <div class="primary-button">
                            <a href="<?php echo base_url(); ?>contact_us">Enquire Now</a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/banner.jpg"
                        class="img-fluid w-100 d-md-block d-none">
                    <img src="<?php echo base_url(); ?>assets/images/mbanner.jpg"
                        class="img-fluid w-100 d-block d-md-none">
                    <div class="banner_text">
                        <h3><span class="text_white">Delivering </span><span class="text_red">better
                                nutrition</span>
                            <span class="text_white">for <br>every step of life’s journey</span>
                        </h3>
                        <div class="primary-button">
                            <a href="<?php echo base_url(); ?>contact_us">Enquire Now</a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/banner.jpg"
                        class="img-fluid w-100 d-md-block d-none">
                    <img src="<?php echo base_url(); ?>assets/images/mbanner.jpg"
                        class="img-fluid w-100 d-block d-md-none">
                    <div class="banner_text">
                        <h3><span class="text_white">Delivering </span><span class="text_red">better
                                nutrition</span>
                            <span class="text_white">for <br>every step of life’s journey</span>
                        </h3>
                        <div class="primary-button">
                            <a href="#">Enquire Now</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
<!--====== banner end ==========-->

<!-- Market Section -->

<section id="team" class="team section-bg top_mr">

    <div class="container px-0">
        <div class="section-title">
            <div class="col-md-12 market-col">
                <h6 class="title1 text-center pb-md-4 py-2"><span class="text_black1">Markets</span>
                    <span class="text_black1">We Serve</span></span>
                </h6>

            </div>
        </div>

        <div class="row no-gutters product_listing d-lg-flex d-none">

            <div class="col-sm-12 col-md-12 col-xl-6  pb-3 market-div">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                            src="<?php echo base_url(); ?>assets/images/1.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/bakery/markets">
                            <h5 class="red"> Bakery and Cereal Products</h5>
                        </a>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid  market1"
                            src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/savory/markets">
                            <h5 class="red">Savory and Snack Products</h5>
                        </a>
                        <p class="grey">We are committed with our aim that incredible flavor, texture and performance
                            can be achieved with simple, familiar, sustainable ingredients, creating products that
                            approve consumers demands.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid  market1"
                            src="<?php echo base_url(); ?>assets/images/7.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/dairy/markets">
                            <h5 class="red">Dairy Products</h5>
                        </a>
                        <p class="grey">We ensure you make a hero-making business with our range of encapsulated
                            additives, flavorings, fortification blends in your dairy products.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                            src="<?php echo base_url(); ?>assets/images/3.jpg " alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/beverages/markets">
                            <h5 class="red"> Beverages</h5>
                        </a>
                        <p class="grey">We believe to achieve our top priorities such as meeting customer demands and
                            empowering client’s businesses along with providing solutions. </p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                            src="<?php echo base_url(); ?>assets/images/5.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/meat/markets">
                            <h5 class="red">Meat and Meat Products</h5>
                        </a>
                        <p class="grey">Perfection in product quality, improved preservation and efficient processing
                            forms the basis for meat manufacturers. Hence, we offer streamlined process solutions with
                            the use of encapsulated ingredients such as salts and acidulants when it comes to meat
                            products.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                            src="<?php echo base_url(); ?>assets/images/2.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/confectionery/markets">
                            <h5 class="red">Confectionery Products</h5>
                        </a>
                        <p class="grey">We are committed with our aim that incredible flavor, texture and performance
                            can be achieved with simple, familiar, sustainable ingredients, creating products that
                            approve consumers demands.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                            src="<?php echo base_url(); ?>assets/images/6.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/infant/markets">
                            <h5 class="red"> Infant Nutrition</h5>
                        </a>
                        <p class="grey">Infants require special foods fortified with various Vitamins & Minerals.
                            Vitamins & Minerals added to food intended for young children are subject to very particular
                            purity criteria.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-xl-6 market-div pb-3">
                <div class="member row no-gutters">
                    <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                            src="<?php echo base_url(); ?>assets/images/8.jpg" alt=""></div>
                    <div class="col-sm-12 col-md-7 member-info">
                        <a href="<?php echo base_url(); ?>market/dietary/markets">
                            <h5 class="red"> Dietary Health Supplements</h5>
                        </a>
                        <p class="grey">At Sudeep Nutrition, we make sure to offer sustainable, high quality, and well
                            researched wide portfolios of Ingredient solutions which are suitable to manufacture all
                            types of Dietary Health supplements.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                        Ingredients</a></p>
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                        Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom Nutrient Blends</a>
                                </p>
                                <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters d-lg-none d-flex">
            <div class="col-md-12">
                <div class="owl-carousel owl-theme market-carousel" id="market-carousel">
                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                                        src="<?php echo base_url(); ?>assets/images/1.jpg" alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Bakery and Cereal Products</h5>
                                    <p class="grey">We ensure you deliver products of a consistently superior
                                        standard
                                        in your
                                        high-throughput bakery with our tailor-made bakery solutions.</p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid  market1"
                                        src="<?php echo base_url(); ?>assets/images/4.jpg" alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Savory and Snack Products</h5>
                                    <p class="grey">We are committed with our aim that incredible flavor,
                                        texture and performance
                                        can be achieved with simple, familiar, sustainable ingredients, creating
                                        products that
                                        approve consumers demands.</p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid  market1"
                                        src="<?php echo base_url(); ?>assets/images/7.jpg" alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Dairy Products</h5>
                                    <p class="grey">We ensure you make a hero-making business with our range of
                                        encapsulated
                                        additives, flavorings, fortification blends in your dairy products.</p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                                        src="<?php echo base_url(); ?>assets/images/3.jpg " alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Beverages</h5>
                                    <p class="grey">We believe to achieve our top priorities such as meeting
                                        customer demands and
                                        empowering client’s businesses along with providing solutions. </p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                                        src="<?php echo base_url(); ?>assets/images/5.jpg" alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Meat and Meat Products</h5>
                                    <p class="grey">Perfection in product quality, improved preservation and
                                        efficient processing
                                        forms the basis for meat manufacturers. Hence, we offer streamlined
                                        process solutions with
                                        the use of encapsulated ingredients such as salts and acidulants when it
                                        comes to meat
                                        products.</p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                                        src="<?php echo base_url(); ?>assets/images/8.jpg" alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Dietary Health Supplements</h5>
                                    <p class="grey">At Sudeep Nutrition, we make sure to offer sustainable, high
                                        quality, and well
                                        researched wide portfolios of Ingredient solutions which are suitable to
                                        manufacture all
                                        types of Dietary Health supplements.</p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide">
                            <div class="member row no-gutters">
                                <div class="col-sm-12 col-md-5 pic"><img class="img-fluid market1"
                                        src="<?php echo base_url(); ?>assets/images/6.jpg" alt=""></div>
                                <div class="col-sm-12 col-md-7 member-info">
                                    <h5 class="red">Infant Nutrition</h5>
                                    <p class="grey">Infants require special foods fortified with various
                                        Vitamins & Minerals.
                                        Vitamins & Minerals added to food intended for young children are
                                        subject to very particular
                                        purity criteria.</p>
                                    <div class="row no-gutters">
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Encapsulated
                                                    Ingredients</a></p>
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Spray Dried
                                                    Ingredients</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p> <a href="<?php echo base_url(); ?>product_list" class="t">Custom
                                                    Nutrient Blends</a></p>
                                            <p><a href="<?php echo base_url(); ?>product_list" class="t">Granulated
                                                    Products</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- End Of Market Section -->


<!-- Product Section Started -->


<section class="product top_mr">
    <div class="container px-0">
        <div class="row no-gutters align-items-center">
            <div class="col-md-5 mr-auto">
                <h2 class="text_dark title1">Our Products</h2>
                <p class="pro1">Accredited with FSSC 22000, EXCiPACT, WHO GMP, ISO 9001:2015, HACCP, Kosher, and
                    Halal/MUI Halal certification we offer products that comply with the regulations and are ideal for
                    every industry.
                </p>

                <a href="<?php echo base_url(); ?>product_list">
                    <p class="text_orange">Explore All Products<span>&#8594;</span></p>
                </a>
            </div>
            <div class="col-md-6">
                <img src="<?php echo base_url(); ?>assets/images/product-1.jpg" alt=""
                    class="img-fluid encapsulated-img">
                <div class="flex-div">
                    <div class="text-block1 text-block2">
                        <p class="orange">Encapsulated Minerals</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="pt-lg-5 mt-lg-4 mt-0 pt-4">
            <div class="row">
                <div class="col-md-6 col-lg-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-2.jpg" alt=""
                        class="img-fluid ingredients-img">
                    <div class="flex-div">
                        <div class="text-block1">
                            <p class="orange">Encapsulated Ingredients</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-3.jpg" alt=""
                        class="img-fluid ingredients-img">
                    <div class="flex-div">
                        <div class="text-block1">
                            <p class="orange">Encapsulated Bakery Ingredients</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-4.jpg" alt=""
                        class="img-fluid ingredients-img">
                    <div class="flex-div">
                        <div class="text-block1">
                            <p class="orange">Encapsulated Nutrients</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-5.jpg" alt=""
                        class="img-fluid ingredients-img">
                    <div class="flex-div">
                        <div class="text-block1">
                            <p class="orange">Encapsulated Food Additives</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</section>



<!-- End of Product Section -->

<!-- Why sudeep nutrition -->

<section class="why extra-mr top_mr mb-5">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class=" col-md-12 col-xl-6 col-lg-6">
                <div class="padding1 py-md-0">
                    <img src="<?php echo base_url(); ?>assets/images/2021-05-30.png" alt="" class="img-fluid">
                    <h3 class="text-white title pt-md-5">Why Sudeep nutrition?</h3>
                    <p class="text1">When you deal with Sudeep Nutrition, the question of quality does not arise. Our
                        customers
                        come to us for the best quality products, especially when their use requires a high level of
                        purity, and regulatory requirements. We have a dedicated team of quality experts that closely
                        controls every process and tests every product in house.</p>
                    <p class="text-white text-font">This commitment of getting each detail correct ensures that Sudeep
                        Nutrition has the highest levels of quality, purity, consistency, and traceability of product.
                    </p>

                    <a href="<?php echo base_url(); ?>sample" class="btn_sec">Request a Sample <span>→</span></a>

                </div>
            </div>
            <div class=" col-md-12 col-xl-4 col-lg-6 ml-auto">
                <div class="row no-gutters">
                    <div class="col-md-6 col-6 bor-right bor-bottom padd-f">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep icons/icon 1.svg" alt=""
                            class="img-fluid d-block m-auto">
                        <p class="prod-text-font mt-3">Best Quality</p>
                    </div>
                    <div class="col-md-6 col-6 bor-bottom padd-f">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep icons/icon 2.svg" alt=""
                            class="img-fluid d-block m-auto">
                        <p class="prod-text-font mt-3">Expert Scientists</p>
                    </div>
                    <div class="col-md-6 col-6 bor-right bor-bottom padd-f">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep icons/icon 3.svg" alt=""
                            class="img-fluid d-block m-auto">
                        <p class="prod-text-font mt-3">Advanced Technology</p>
                    </div>
                    <div class="col-md-6 col-6 bor-bottom padd-f">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep icons/icon 4.svg" alt=""
                            class="img-fluid d-block m-auto">
                        <p class="prod-text-font mt-3">Best Price</p>
                    </div>
                    <div class="col-md-6 col-6 bor-right padd-f">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep icons/icon 5.svg" alt=""
                            class="img-fluid d-block m-auto">
                        <p class="prod-text-font mt-3">Premium raw material</p>
                    </div>
                    <div class="col-md-6 col-6 padd-f">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep icons/icon 6.svg" alt=""
                            class="img-fluid d-block m-auto">
                        <p class="prod-text-font mt-3">High-Tech Labs</p>
                    </div>
                </div>
            </div>
            <!-- 
            <div class="col-md-3">
                <div class="why-bx1">
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep 1.svg" alt="" class="img-fluid">
                        <p class="t1">Best Quality</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep 2.svg" alt="" class="img-fluid">
                        <p class="t1">Expert Scientists</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep 3.svg" alt="" class="img-fluid">
                        <p class="t1">Advanced Technology</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <div class="why-bx2">
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep 4.svg" alt="" class="img-fluid">
                        <p class="t1">Best Price</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep 5.svg" alt="" class="img-fluid">
                        <p class="t1">Premium raw material</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why sudeep 6.svg" alt="" class="img-fluid">
                        <p class="t1">High-Tech Labs</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
</section>


<!-- End Of Why sudeep -->


<!-- Latest Update Start -->
<!-- <div class="format"> -->
<!-- <section class="update top_mr">
    <div class="conatiner px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="tag text-center title1">Latest Updates and News</h2>
            </div>
        </div>


        <main>
            <div class="container bg-trasparent my-md-4 my-2 p-1 px-0" style="position: relative;">
                <div class="row no-gutters">
                    <div class=" col-sm-6 col-md-6 col-lg-3">
                        <div class="card1  card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 1.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Category Name | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="card1 card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 2.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Instagram | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="card1 card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 3.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Linked In | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="card1 card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 4.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Youtube | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </main>
    </div>
</section> -->
<!-- </div> -->

<!-- End of latest Update -->
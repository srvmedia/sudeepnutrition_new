<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sorbicacid.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">


</head>



<!-- Humberger Navbar -->


<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Hot melt extrusion/banner.jpg"
                        class="img-fluid w-100 pb-5">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">Hot melt extrusion</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="content1 pt-5 pb-5 ">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h5 class="color-green green-pdd text-center">Hotmelt extrusion is a novel and promising technology used for
                    microencapsulation of flavors and bioactive compounds because of its robust, viable method and
                    highly efficient process.
                    It has been widely used in solubility enhancement applications for dispersing the active substance
                    in polymer or lipid matrices at the molecular level.</h5>
            </div>
        </div>
    </div>
</section>

<section class="mission pt-5 pb-5">
    <div class="container-fuild px-0">
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section">
                <div class="para-div1">
                    <h5 class="color-blue padd-p">How does it work?</h5>
                    <p class="mech-text">In Hot Melt extrusion, the active substance/ ingredient with one or more excipients
                    such as polymers,
                    lipids, surfactants, diluents, lubricants, glidants, plasticizers and other modifiers are fed into
                    heated processing zones of the extruder.</p>
                    <p class="mech-text"> This mixture is then processed at high temperature and pressure. Modular twin
                    co-rotating screws
                    impart a high degree of shear and mixing into the molten mixture, creating a homogenous mass and
                    finally shapes the mass by pressing them through a die opening. While exiting the extruder, the
                    molten homogenous extrudate is rapidly cooled and solidified.</p>
                    <p class="mech-text"> Hot Melt extrusion is suitable for both low and high-potent compounds and has been
                    demonstrated to
                    provide stabilization, bioavailability enhancement, controlled release or taste masking and
                    dust-free processing. These applications demonstrate the potential of Hot Melt extrusion as an
                    extended and time reaction processing technology.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Hot melt extrusion/intro 1.jpg" alt="" class="img-fluid section1">
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section img-hide">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Hot melt extrusion/intro 2.jpg" alt="" class="img-fluid section1">
            </div>
            <div class="col-md-6 px-0 col-section">
                <div class="para-div1">
                <h5 class="color-blue padd-p">Advantages</h5>
                <ul class="st1 listicon mech-text">
                    <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                    Provide Targeted delivery with a consistent and constant concentration of the active substance
                        with fewer side effects.</li>
                    <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                    It is used as a taste-masking technique for bitter active ingredients by the formulation of
                        solid dispersions with a taste-masking polymer.</li>
                    <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                    Sustained-release formulation needed to employ the release of specific active ingredients at a
                        predetermined time temperature.</li>
                    <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                    Processing involves no solvent requirement hence solvent-related stability risks that during the
                        shelf life of the formulation can be avoided since there is no residual solvent.</li>
                    <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                    Dust-free manufacturing that utilizes only a limited number of excipients and low-cost
                        instrumentation.</li>
                </ul>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section img-show">
                <img src="<?php echo base_url(); ?>assets/images/advantages.jpg" alt="" class="img-fluid section1">
            </div>
        </div>
    </div>
</section>




</body>

</html>
<div class="container px-0 pt-5">
    <div class="row no-gutters justify-content-center top-part">
        <div class="col-md-6 text-center center-content">
            <div class="d-flex-inline">

                <img src="<?php echo base_url(); ?>assets/images/footer logo.png" alt=""
                    class="img-fluid ft footer_logo">
            </div>
            <div class="row no-gutters justify-content-center mt-3">
                <img src="<?php echo base_url(); ?>assets/images/social media 5.svg" alt="" class="img-fluid">
                <img src="<?php echo base_url(); ?>assets/images/social media 4.svg" alt="" class="img-fluid">
                <img src="<?php echo base_url(); ?>assets/images/social media 3.svg" alt="" class="img-fluid">
                <img src="<?php echo base_url(); ?>assets/images/social media 2.svg" alt="" class="img-fluid">
                <img src="<?php echo base_url(); ?>assets/images/social media 1.svg" alt="" class="img-fluid">
            </div>
            <hr>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="line mb-3 mx-auto d-md-block d-none"></div>
        <div class="col">
            <div class="d-flex justify-content-around  col-md-12 pt-md-5 mb-md-3">
                <div class="flex-fill bd-highlight mb-3 mb-md-0">
                    <h3 class="f1">About us</h3>
                    <div class="para">
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>what_we_do">What we do</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">What Sevices we provide</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>about_us">Our Mission</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>about_us">Innovation</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>about_us">Responsibility (CSR)</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>about_us">Board Of Directors</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>join_team">Join Our team</a></p>
                    </div>
                </div>
                <div class="flex-fill bd-highlight mb-3 mb-md-0">
                    <h3 class="f1">Technologies</h3>
                    <div class="para">
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>microencapsulation">Micro
                                Encapsulation</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>hotmelt">Hot Melt Extrusion</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>spray">Drying</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>blending">Blending</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>granulation">Granulation</a></p>
                    </div>
                </div>
                <div class="flex-fill bd-highlight mb-3 mb-md-0">
                    <h3 class="f1">Markets</h3>
                    <div class="para">
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/bakery/markets">Bakery and Cereal
                                Products</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/savory/markets">Savoury and Snack
                                Products</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/dairy/markets">Dairy Products</a>
                        </p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/beverages/markets">Beverages</a>
                        </p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/meat/markets">Meat and Meat
                                Products</a></p>
                        <p class="footer-mr"><a
                                href="<?php echo base_url(); ?>market/confectionery/markets">Confectionery Products</a>
                        </p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/infant/markets">Infant
                                Nutrition</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>market/dietary/markets">Dietary Health
                                Supplements</a></p>
                    </div>
                </div>
                <div class="flex-fill bd-highlight mb-3 mb-md-0">
                    <h3 class="f1">Quality & product safety</h3>
                    <div class="para">
                        <p class="footer-mr"> <a href="<?php echo base_url(); ?>">Accreditations</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">Certifications</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">Compliances</a></p>
                    </div>

                </div>

                <div class="flex-fill bd-highlight mb-0 mb-md-0 d-lg-block d-none">
                    <h3 class="f1">Resource Centre</h3>
                    <div class="para">
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>sample">Request A Sample</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>what_we_do">Your Application Expert
                                Product Development</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">Product Information Services</a></p>
                    </div>
                    <div class="row mt-md-0 mt-3">
                        <div class="p-md-4 flex-fill bd-highlight">
                            <h3 class="f1">Media</h3>
                            <div class="para">
                                <p class="footer-mr"> <a href="<?php echo base_url(); ?>">Articles</a> </p>
                                <p class="footer-mr"><a href="<?php echo base_url(); ?>">What’s in the News?</a> </p>
                                <p class="footer-mr"><a href="<?php echo base_url(); ?>">Events</a> </p>
                                <p class="footer-mr"><a href="<?php echo base_url(); ?>">Press Release</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex-fill bd-highlight mb-0 mb-md-0 d-lg-none d-block">
                    <h3 class="f1">Resource Centre</h3>
                    <div class="para">
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>sample">Request A Sample</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>what_we_do">Your Application Expert
                                Product Development</a></p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">Product Information Services</a></p>
                    </div>
                </div>
                <div class="flex-fill bd-highlight mb-3 mb-md-0 d-lg-none d-block">
                    <h3 class="f1">Media</h3>
                    <div class="para">
                        <p class="footer-mr"> <a href="<?php echo base_url(); ?>">Articles</a> </p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">What’s in the News?</a> </p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">Events</a> </p>
                        <p class="footer-mr"><a href="<?php echo base_url(); ?>">Press Release</a></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="line mb-3 mx-auto"></div>
</div>

<!-- <div class="row no-gutters color-grey  pt-2">
        <div class="col-md-6">
            <h6 style="font-size: 0.8rem;">© 2020 Mockup. All Rights Reserved.</h6>
        </div>
        <div clas="col-md-6">
            <h6 style="font-size: 0.8rem;">Privacy Policy Terms of Service</h6>
        </div>
    </div> -->

<div class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="mb-0 py-md-4 py-2">© 2021 Sudeep Nutrition. All Rights Reserved.<a href="" target="_blank"
                        class="text-light"></a> &nbsp | &nbsp<a href="" style="cursor: pointer;"> Privacy Policy
                </p>
            </div>

        </div>
    </div>
</div>

</div>
<!-- <button class="btn scroll" id="scroll1" style="display: none;">Request a Sample</button> -->

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/aos.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/js/counterup.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/js/jquery.waypoints.min.js"></script> -->



<script src="<?php echo base_url(); ?>assets/owlCarousel/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/code.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/utm_code.js"></script> -->



<script>
// jQuery counterUp
// $('[data-toggle="counter-up"]').counterUp({
//     delay: 10,
//     time: 1000
// });
</script>

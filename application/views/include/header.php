<body>
<nav class="navbar navbar-light navbar-expand-lg w-100 sticky-top bg-white shadow" id=header>
    <div class="container px-0">
        <a class="navbar-brand float-md-left" href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/images/header logo.png" class="img-fluid header-logo">
        </a>
        <!-- <button class="navbar-toggler float-right mt-1" type="button" data-toggle="collapse"
            data-target="#collapsibleNavbar">
        </button> -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav  ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle lik say"   id="navbartechnologies" role="button" data-toggle="dropdown" aria-expanded="false">Technologies</a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbartechnologies">
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>technology">Encapsulation</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>microencapsulation">Microencapsulation</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>hotmelt">Hot melt extrusion</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>spray">Spray Drying</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>granulation">Granulation</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>blending">Blending</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle lik say"  id="navbarmarket" role="button" data-toggle="dropdown" aria-expanded="false">Markets</a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarmarket">
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/bakery/markets">Bakery and Cereal Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/savory/markets">Savory and Snack Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/dairy/markets">Dairy Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/beverages/markets">Beverages</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/meat/markets">Meat and Meat Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/confectionery/markets">Confectionery Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/infant/markets">Infant Nutrition</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/dietary/markets">Dietary Health Supplements</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link lik say" href="<?php echo base_url(); ?>product_list">Products<a>
                </li>

            </ul>
        </div>

        <!-- <a class="navbar-brand  m1 d-none d-lg-block " href="#">
            
            <img src="images/hamburger menu.svg" class="img-fluid">

        </a> -->
        <div id="mySidepanel" class="sidepanel">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
            <div class="dropdown d-lg-none d-block">
                <a href="#home" class="dropdown-btn">Technologies <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-content" style="display:none"> 
                <li><a class="dropdown-item" href="<?php echo base_url(); ?>technology">Encapsulation</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>microencapsulation">Microencapsulation</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>hotmelt">Hot melt extrusion</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>spray">Spray Drying</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>granulation">Granulation</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>blending">Blending</a></li>
                </div>
            </div>
            <div class="dropdown d-lg-none d-block">
                <a href="#home" class="dropdown-btn">Markets<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-content" style="display:none"> 
                <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/bakery/markets">Bakery and Cereal Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/savory/markets">Savory and Snack Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/dairy/markets">Dairy Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/beverages/markets">Beverages</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/meat/markets">Meat and Meat Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/confectionery/markets">Confectionery Products</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/infant/markets">Infant Nutrition</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>market/dietary/markets">Dietary Health Supplements</a></li>
                </div>
            </div>
            <div class="dropdown d-lg-none d-block">
                <a href="<?php echo base_url(); ?>product_list" class="">Products</a>
            </div>

            <div class="dropdown">
                <a href="#home" class="dropdown-btn">About Us <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-content" style="display:none"> 
                <a class="nav-subTitle" href="<?php echo base_url(); ?>what_we_do" title="">What we do</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>what_services" title="">What Sevices we provide</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>about_us" title="">Our Mission</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>about_us" title="">Innovation</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>about_us" title="">Responsibility (CSR)</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>about_us" title="">Board Of Directors</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>join_team" title="">Join Our team</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">Resource Centre<i class="fa fa-angle-down"
                        aria-hidden="true"></i></a>
                <div class="dropdown-content" style="display:none">
                <a class="nav-subTitle" href="<?php echo base_url(); ?>sample" title="">Request A Sample</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>what_we_do" title="">Your Application Expert Product Development</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>product_information" title="">Product Information Services</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">Quality & product safety<i class="fa fa-angle-down"
                        aria-hidden="true"></i></a>
                <div class="dropdown-content" style="display:none">
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>certification" title="">Accreditations</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>certification" title="">Certifications</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>compliance" title="">Compliances</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">Media<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-content" style="display:none">
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>" title="">Articles</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>" title="">What’s in the News?</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>" title="">Events</a>
                    <a class="nav-subTitle" href="<?php echo base_url(); ?>" title="">Press Release</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="<?php echo base_url(); ?>contact_us" class="btn_sec1">Contact Us</a>
            </div>
        </div>





        <!-- <button class="dropdown-btn">About Us<i class="fa fa-angle-down" aria-hidden="true"></i></button> -->
        <!-- <button class="dropdown-btn">Resource Centre<i class="fa fa-angle-down" aria-hidden="true"></i></button>
            <button class="dropdown-btn">Quality & product safety<i class="fa fa-angle-down"
                    aria-hidden="true"></i></button>
            <button class="dropdown-btn">Media<i class="fa fa-angle-down" aria-hidden="true"></i></button>
            <a href="#home" class="btn_sec">Contact <span>→</span></a> -->
            <button class="openbtn" onclick="openNav()"><img src="<?php echo base_url(); ?>assets/images/hamburger menu.svg"
            class="img-fluid"></button>
            <!-- <button class="openbtn" onclick="openNav()"><img src="<?php echo base_url(); ?>assets/images/hamburger menu.svg"
            class="img-fluid d-block d-lg-none"></button> -->

    </div>

    <!-- <div id="main">
            <button class="openbtn" onclick="openNav()"><img src="<?php echo base_url(); ?>assets/images/hamburger menu.svg" class="img-fluid"></button>
            
        </div> -->
    <script>
    function openNav() {
        document.getElementById("mySidepanel").style.width = "350px";
    }

    function closeNav() {
        document.getElementById("mySidepanel").style.width = "0";
    }

    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
        });
    }
    // window.onscroll = function() {
    //     myFunction()
    // };
    </script>
    </div>
</nav>
<!-- Humberger Navbar -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/Assets/Product detail page- Encapsulated Sorbic acid/banner.jpg"
                        class="img-fluid w-100 pb-5">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/" class="img-fluid w-100 d-block d-lg-none"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">Spray Drying</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="content1 pt-5 pb-5 ">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h5 class="color-green text-center green-pdd">Spray Drying is a one-step process established processing
                    technology
                    for products widely used in infant formulations, powdered sweets and cocoa soluble in milk for
                    children, or food supplements rich in proteins, vitamins, and minerals for adults, fats and oils,
                    and flavorings and colorings in a spray-dried form. The technology is favorable because it is
                    inexpensive, efficient and can be used in continuous operation and produces particles of good
                    quality.</h5>
            </div>
        </div>
    </div>
</section>

<section class="mission pt-5 pb-5">
    <div class="container-fuild px-0">
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section order-xl-1 order-2">
                <div class="para-div1">
                    <h5 class="color-blue padd-p">How does it work?</h5>
                    <p class="mech-text">The process involves forming a stable emulsion of active substance and polymer
                        and
                        changing this emulsion from a liquid form into a powder in a continuously operating procedure.
                        The
                        liquid droplets are atomized with a specialized nozzle and further sprayed into a hot drying gas
                        to
                        form the required size of particles.</p>
                    <p class="mech-text"> Spray-drying optimizes dissolution rates and improves the bioavailability of
                        poorly
                        soluble compounds. It is a widely used processing technology, spray-drying offers several
                        advantages
                        over conventional multi-step processes and particle reduction technologies.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section px-xl-0 px-3 mb-xl-0 mb-3  order-xl-2 order-1">
                <img src="<?php echo base_url(); ?>assets/images/Assets/Product detail page- Encapsulated Sorbic acid/why it works.jpg"
                    alt="" class="img-fluid section1">
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-6 px-0 col-section img-hide">
                <img src="<?php echo base_url(); ?>assets/images/advantages.jpg" alt="" class="img-fluid section1">
            </div>
            <div class="col-md-6 px-0 col-section order-xl-1 order-2"">
                <div class="para-div1">
                    <h5 class="color-blue padd-p">Advantages</h5>
                    <ul class="st1 listicon mech-text">
                        <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                            Creation of an amorphous solid dispersion</li>
                        <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                            Ability to modify product specified, particle properties from very fine particles to
                            agglomerated powders.</li>
                        <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                            Fast and continuous process</li>
                        <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                            Suitable for heat-sensitive products</li>
                        <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                            Processing of aqueous and organic solvent systems</li>
                        <li><img src="<?php echo base_url(); ?>assets/images/bullet.svg" alt="">
                            Predictable scale-up</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-section img-show px-xl-0 px-3 mb-xl-0 mb-3  order-xl-2 order-1">
                <img src="<?php echo base_url(); ?>assets/images/advantages.jpg" alt="" class="img-fluid section1">
            </div>
        </div>
    </div>
</section>
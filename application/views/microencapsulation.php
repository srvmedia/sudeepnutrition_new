
 <style type="text/css">

    .img-bg {
      width: auto !important;
}
h5.color-green {
    color: #009847;
    font-weight: bold;
    font-size: 1.8rem;
}
.text-p {
    color: #212121;
    font-weight: bold;
    font-size: 1rem;
}
h1.encap-text{
    font-size: 2.5rem;
    font-weight: 600;
}


.mech-text {
    font-size: 1.4rem;
        color: #2C5B8D;
        font-weight: 600;
}
ul.listicon {
    list-style: none;
    padding: 0;
}
ul li{
    font-size: 1.2rem;
}

/*responsive part starts*/
@media screen and (max-width: 767px) {
     .banner_text h3 {
    font-size: 1.75rem !important;
  }
}
/*responsive part ends*/
    </style>
<!-- Humberger Navbar -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/banner.jpg"
                        class="img-fluid w-100 pb-5">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/banner3.jpg" class="img-fluid w-100 d-none d-lg-block pb-5"> -->
                     <!-- <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/banner3m.jpg" class="img-fluid w-100 d-block d-lg-none pb-5"> -->
                    <div class="banner_text">
                        <div class="primary-button pt-3">
                        </div>
                        <h3 class="text-white">Microencapsulation Technology</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="content1 pt-5 pb-5">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h5 class="color-green text-center green-pdd">Microencapsulation involves coating an active substance with a polymetric material and forming a microencapsulated product (microparticles,
                microcapsules and microspheres) having diameter between 1 to 1000 μm.</h5>
            </div>
        </div>
    </div>
</section>

<section class="mission">
    <div class="container px-0">
        <div class="row no-gutters  d-flex align-items-center">
            <div class="col-md-6 px-0 col-section">
                <div class="para-div1">
                    <p class="mech-text">Types of encapsulation technologies employed for encapsulation</p>
                    <ul class="listicon">
                        <li> <img src="https://staging-1.srv.media/web/sudeepnutrition_new/assets/images/Assets/Product detail page- Encapsulated Sorbic acid/bullet.svg" alt=""> Micro- encapsulation using Hot melt Coating</li>
                        <li> <img src="https://staging-1.srv.media/web/sudeepnutrition_new/assets/images/Assets/Product detail page- Encapsulated Sorbic acid/bullet.svg" alt=""> Micro-encapsulation using Spray drying</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section">
                <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/intro.jpg" alt="" class="img-fluid section1">
            </div>
        </div>
    </div>
</section>
<section class="info-section mb-5">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-lg-12">
               <h1 class="encap-text">How do you decide to choose a technology for encapsulation?</h1>
            </div>
            <div class="col-lg-12 pb-5">
               <div class="row d-flex justify-content-center">
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/icon 1.svg" class="img-fluid img-bg">
                                <p class="text-p">Characteristic of the active substance (liquid, solid, volatile), Hydrophobic, Hydrophilic.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/icon 2.svg" class="img-fluid img-bg">
                                <p class="text-p">Requirement of end use application</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="<?php echo base_url(); ?>assets/images/Technologies/Microencapsulation/icon 3.svg" class="img-fluid img-bg">
                                <p class="text-p">Material for isolating the active substance.</p>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div> 
    </div>  
</section>



<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-dark bg-primary">
                <div class="container-fluid">
                    <div class="navbar-header" style="margin-left:3%;">
                        <a class="navbar-brand" href="#" style="color:#fff;">Admin Login</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-12">
            </div>
            <div class="col-lg-6 col-md-4 col-sm-12">
                <h2 class="text-center">Admin Login</h2>
                <div class="panel panel-primary">
                    <div class="panel-heading">Please enter credentials to login</div>
                    <div class="panel-body">
                        <div id="blank" class="text-danger"></div>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="username" class="form-control" id="username">
                            <div id='error' class="text-danger"></div>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd">
                            <div id='error1' class="text-danger"></div>
                        </div>
                        <button type="submit" class="btn btn-primary checklogin">Login</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12">
            </div>
        </div>
    </div>
</body>
<script>
$(document).ready(function() {
    $('.checklogin').click(function() {
        $('#error').html();
        $('#error1').html();
        $('#blank').html('');
        var username = $('#username').val();
        var pwd = $('#pwd').val();


        // if(!username)
        // {
        //     $('#error').append('Please enter username');
        //     return false;
        // }
        // else if (/[^a-zA-Z \-]/.test( username ))
        //   {

        //    $('#error1').html("Enter only alphabates in first name");
        //     return false;
        //   }
        // else if(!pwd)
        // {
        //     $('#error1').append('Please enter password');
        //     return false;
        // }
        // else
        // {

        $.ajax({
            method: "POST",
            url: "checklogin.php",
            data: {
                'username': username,
                'password': pwd
            },
            cache: false,
            success: function(data) {
                var data = JSON.parse(data);

                if (data.status == 'blank') {
                    $('#blank').html('');
                    $('#blank').append('Please enter credentials.');
                } else if (data.status == 'incorrect') {
                    $('#blank').html('');
                    $('#blank').append('Please enter correct credentials.');
                } else if (data.status == 'correct') {
                    window.location.href = 'dashboard.php';
                } else {
                    $('#blank').html('');
                    $('#blank').append('Please try later.');
                }

            }
        });
        //}

    });
});
</script>

</html>
<?php

if (isset($_POST['username']) && isset($_POST['password'])) {

    if ($_POST['username'] == '' && $_POST['password'] == '') {
        $return = array('status' => 'blank');
        echo json_encode($return);
    } else {
        $username = trim(htmlspecialchars($_POST['username']));
        $password = trim(htmlspecialchars($_POST['password']));


        if ($username == 'root' && $password == 'root') {
            session_start();
            $_SESSION['logged-in'] = true;
            $_SESSION['logged-role'] = 'admin';
            $return = array('status' => 'correct');
            echo json_encode($return);
        } else {
            $return = array('status' => 'incorrect');
            echo json_encode($return);
        }
    }
} else {
    $return = array('status' => 'blank');
    echo json_encode($return);
}
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


<style type="text/css">

/*style css starts*/

body{
    overflow-x:hidden;
}

.text-red01{
    color:#E31F25;
}

.banner_text02 {
  position: absolute;
  width: 84%;
  top: 55%;
  left: 4%;
  transform: translatey(-50%);
  z-index: 999;
  bottom: 23%;
}
.text-blue03{
    color:#2C5B8D;
    font-weight: bold;
}
.text-black03{
    color: #030303;
    font-size: 1.2rem;
}

.btn-secondary, .btn-secondary:hover {
    color: #fff;
    background-color: #F07F1B;
    border-color: #F07F1B;
    border-radius: 1rem;
    text-transform: capitalize;
}
.product{
    background: #F8F8F8;
    padding: 1rem 0rem;
    margin-top: 3rem;
}

.prod-text{
    font-size: 1.2rem;
    text-align: center;
}

.bor-r{
    border-right: 1px solid #e4e4e4;
}
.bor-b{
    border-bottom: 1px solid #e4e4e4;
}

.product img {
    padding-right: 0rem !important;
}

.prod-text2{
    position: relative;
    top: -25%;
    left: 10%;
    color: white;
    font-weight: bold;
    font-size: 1.2rem;
    cursor: pointer;
}
.mtprod{
    margin-top: -3.8rem;
}

.bakery-bg{
    background: linear-gradient(90deg,#2C5B8D,#103E6F);
    box-shadow: 0px 30px 35px #0000002e;
    border-radius: 0.8rem;
}

.para7{
    font-size: 1.4rem;
}

.prod-text3 {
    position: relative;
    left: -96%;
    color: white;
    font-weight: bold;
    font-size: 1.8rem;
    bottom: -14rem;
}
.close {
    position: relative;
    left: -13rem;
    top: 2rem;
    opacity: 1;
}
button.close {
    background: white;
    height: 1px;
    align-items: center;
    display: flex;
    border-radius: 50%;
    width: 1px;
    justify-content: center;
}

.tabhead{
    color: #F07F1B;
}
.modal-dialog {
        max-width: 700px !important;
}
.modal-body{
    padding: 2rem;
}

/*style css ends*/


/*Responsive style css starts*/

@media only screen  and (min-width: 768px) and (max-width:990px){
.popular-product p {
    font-size: 0.65rem;
}
.mtprod {
    margin-top: -3rem;
}
}
@media only screen  and (min-width: 991px) and (max-width:1279px){
.popular-product p {
    font-size: 0.85rem;
}
}
@media only screen  and (max-width: 767px){
  .bor-rn{
    border-right: none;
}
.bor-bd{
    border-bottom: 1px solid #e4e4e4;
}
.mt1{
    margin-top: 3rem !important;
}
.banner_text02 h5{
    font-size:0.8rem;
}
}
@media only screen  and (max-width: 319px){
.banner_text02 h5{
    font-size:0.45rem;
}
}

@media only screen  and (min-width: 1025px){
.container-custom {
    padding: 0rem 7rem 0rem 7rem;
}
}

@media only screen and (max-width: 577px){
.res_cust {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
    text-align: center;
}
.prod-text2 {
   left: -10%;
 }
 .mtprod2{
    margin-top: -3.8rem;
}
.reshide{
    display:none;
}

}
@media only screen and (min-width: 768px){
  .reshide{
    display:none;
}
}
@media only screen and (min-width: 576px) and (max-width: 767px){
.res_cust {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}

.mtprod3{
    margin-top: -3.8rem;
}
.reshide2{
    display:none;
}
.mtprod4{
    margin-top: 0rem;
}
}
@media only screen and (max-width: 1100px){
.modal-dialog {
    max-width: 500px !important;
}
}
/*Responsive style css ends*/

</style>

</head>

<!-- Humberger Navbar -->




<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
  
                     <img src="<?php echo base_url(); ?><?php echo $main_header[0]->image; ?>" class="img-fluid w-100 d-none d-lg-block">
                     <img src="<?php echo base_url(); ?><?php echo $main_header[0]->mobile_image; ?>" class="img-fluid w-100 d-block d-lg-none">
                                       
                        <div class="banner_text02">
                        <h1 class="text-red01 font-weight-bold"><?php echo $main_header[0]->sub_section_header; ?></h1>
                        <h5 class="text-white font-weight-bold mb-3"><?php echo $content[0]->header; ?>
                        </h5>
                        </div>
</section>
<!--====== banner end ==========-->


<section class="whysudeep">
    <div class="container-fluid container-custom">
        <div class="row mt-5">
            <div class="col-lg-6 col-12 mt-4">
                <h1 class="text-blue03">Why Sudeep Nutrition?</h1>
                <p class="text-black03 mt-4"><?php echo $content[0]->sub_header; ?></p>
                <button type="button" class="btn btn-secondary btn-sm my-3 font-weight-bold">Enquire Now  <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow.svg" class="img-fluid pl-2"></button>
            </div>
            <div class="col-lg-6 col-12">
                <img src="<?php echo base_url(); ?><?php echo $content[0]->image; ?>" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>


<section class="product">
    <div class="container-fluid container-custom">
          <h1 class="text-blue03 text-center mt-5">We help you overcome everyday product<br> challenges like</h1>
          <div class="row mt-5">
          <?php foreach ($challenges as $challenge) { ?>
            <div class="col-md-3 col-6 bor-r bor-b">
                  <img src="<?php echo base_url(); ?><?php echo $challenge->image; ?>" class="img-fluid d-block m-auto img-bg">
                  <p class="prod-text mt-3"><?php echo $challenge->challenges; ?>
              </div>

          <?php } ?>
          </div>
    </div>
</section>

<section class="popular-product">
    <div class="container-fluid">
          <h1 class="text-blue03 text-center mt-5">Our Most Popular Products</h1>
          <?php $count=0; ?>
          <?php foreach (array_chunk($products,5) as $array_product) { ?>
            <?php if($count==0) : ?>
                    <div class="row no-gutters">
                <?php endif; ?>
                <?php if($count==1) : ?>
                    <div class="row no-gutters mtprod">
                <?php endif; ?>
            <?php foreach ($array_product as $product) { ?>
                <div class="col res_cust">
                    <img src="<?php echo base_url(); ?><?php echo $product->image; ?>" class="img-fluid">
                    <p class="prod-text2 text-white mt-3" data-toggle="modal" data-target="#myModal<?php echo $product->id; ?>"><?php echo $product->label; ?> <img src="<?php echo base_url(); ?><?php echo $product->arrow_image; ?>" class="img-fluid pl-2"></p>
                </div>
                <?php $count=1; ?>
            <?php } ?>
            </div>
            <?php } ?>
            </div>
    </div>
</section>


<section class="bakery mb-5">
    <div class="container">
          <div class="row bakery-bg align-items-center">
            <div class="col-lg-1"></div>
             <div class="col-lg-7">
                 <h1 class="text-white mt-5 font-weight-bold">Encapsulated Bakery Ingredients Annexure</h1>
                 <p class="text-white mt-3 para7">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do eiusmod tempor erto incididunt ut labore et dolore magna gusta consectetur eyter.</p>
                 <button type="button" class="btn btn-secondary btn-sm my-3 font-weight-bold">Download PDF <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/pdf.svg" class="img-fluid pl-2"></button>
             </div>
            <div class="col-lg-3">
                <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/img2.png" class="img-fluid">
            </div>
            <div class="col-lg-1"></div>
          </div>
           
    </div>
</section>

  <!-- The Modal -->
  <div class="modal" id="myModal1">
    <div class="modal-dialog modal-dialog-centered modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header p-0">
          <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/popup.jpg" class="img-fluid">
          <p class="prod-text3 text-white mt-3">Preservatives </p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <h4 class="font-weight-bold tabhead">Products</h4>
          <hr>
          <div class="row">
              <div class="col-md-6">
                  <h6><a href="#"><u>Sorbic Acid </u><img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                  <h6><a href="#"><u>Malic Acid</u> <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                  <h6>Fumaric Acid </h6>
                  <h6>Tartaric Acid</h6>
                  <h6>Citric Acid 50%</h6>
                  <h6>Citric Acid DC </h6>
                  <h6>Citric Acid coated with citrate</h6>
                  <h6>Citric Acid with SiO2 </h6>
                  <h6><a href="#"><u>Trisodium phosphate</u> <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                   <h6>Potassium propionate</h6>
              </div>
               <div class="col-md-6 mt-3">
                  <h6>Sodium propionate </h6>
                  <h6>Propionic acid </h6>
                  <h6>Sodium acetate Sodium & Calcium Propionate </h6>
                  <h6>Sodium & Potassium Benzoate </h6>
                  <h6>Sodium, Potassium & Calcium Sorbate</h6> 
                  <h6>Cassia Bark Extract </h6>
                  <h6>Kaffir Lime Leaf Extract</h6> 
                  <h6>Glucose Oxidase</h6>
               </div>
          </div>
        </div>
        
              
      </div>
    </div>
  </div>

  <div class="modal" id="myModal11">
    <div class="modal-dialog modal-dialog-centered modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header p-0">
          <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/popup.jpg" class="img-fluid">
          <p class="prod-text3 text-white mt-3">Preservatives </p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <h4 class="font-weight-bold tabhead">Products</h4>
          <hr>
          <div class="row">
              <div class="col-md-6">
                  <h6><a href="#"><u>Sorbic Acid </u><img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                  <h6><a href="#"><u>Malic Acid</u> <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                  <h6>Fumaric Acid </h6>
                  <h6>Tartaric Acid</h6>
                  <h6>Citric Acid 50%</h6>
                  <h6>Citric Acid DC </h6>
                  <h6>Citric Acid coated with citrate</h6>
                  <h6>Citric Acid with SiO2 </h6>
                  <h6><a href="#"><u>Trisodium phosphate</u> <img src="<?php echo base_url(); ?>assets/images/Assets/Market detail page- Bakery/arrow2.svg" class="img-fluid pl-2"></a></h6>
                   <h6>Potassium propionate</h6>
              </div>
               <div class="col-md-6 mt-3">
                  <h6>Sodium propionate </h6>
                  <h6>Propionic acid </h6>
                  <h6>Sodium acetate Sodium & Calcium Propionate </h6>
                  <h6>Sodium & Potassium Benzoate </h6>
                  <h6>Sodium, Potassium & Calcium Sorbate</h6> 
                  <h6>Cassia Bark Extract </h6>
                  <h6>Kaffir Lime Leaf Extract</h6> 
                  <h6>Glucose Oxidase</h6>
               </div>
          </div>
        </div>
        
              
      </div>
    </div>
  </div>

</body>

</html>

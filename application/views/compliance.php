<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sorbicacid.css">
   
    <!-- <link rel="stylesheet" href="styles/jon_our_team.css"> -->
</head>

<div class="col-md-12">
                <h5 class="app text-center pt-5">Legal Notice</h5>
</div>

<div class="row no-gutters">
            <div class="col-md-12 px-0 col-section">
                <div class="para-div1">
                    <h5 class="h4" style="margin-top:3rem;"><strong>1. General Information</strong> </h5>
                      <p class="mech-text">In compliance with the requirements of Law 34/2002 on Services of the Information Society and Electronic Commerce (LSSICE) we inform you
                      of the following:</p>
                      <p class="mech-text">The presence of a commercial website on the Internet, even if it is only informative, makes the owner company a provider of information society
                      services, being obliged to have the means that allow, both the recipients of the service, To the competent media, access by electronic means,
                      permanently, easily, directly and free of charge, the data that allow their identification:</p>
                      <p class="mech-text">Sudeep Nutrition Pvt. Ltd</p>
                      <p class="mech-text">Address:</p>
                      <p class="mech-text">CIF:</p>
                      <p class="mech-text">Tel.</p>
                      <p class="mech-text">Email:</p>
                      <p class="mech-text">Registered Office:</p>

                      <h5 class="h4" style="margin-top:3rem;"><strong>2.- General Conditions of Use and Acceptance of the same</strong> </h5>
                      <p class="mech-text">The purpose of these general conditions of use of the website is to inform users of Sudeep Nutrition’s activity and products. This website is merely
                      informative.</p>
                      <p class="mech-text">The navigation and the use of the services of the web page imply the acceptance as a User, and without reservations of any kind, of all the general
                      conditions of use, the general contracting conditions, if any, and any others that may exist in relation to with the provision of Sudeep Nutrition
                      services who may at any time and without prior notice, modify these general conditions, providing information to the user in any case through this
                      website.</p>
                      <p class="mech-text">The user of this website must refrain, in any case, from deleting, altering, evading or manipulating any protection device or security system that
                      may be installed in it. The user will respond to Sudeep Nutrition and / or to third parties, for any damages that may be caused as a result of the
                      breach of said obligation.</p>

                      <h5 class="h4" style="margin-top:3rem;"><strong>3.- Industrial and Intellectual Property. Hyperlink policy</strong> </h5>
                      <p class="mech-text">All the contents included in the web page and in particular the brands, trade names, industrial designs, designs, texts, photographs, images,
                      illustrations, graphics, logos, icons, software or any other signs susceptible of industrial and commercial use are protected by Industrial and
                      intellectual property rights of Sudeep Nutrition Pvt. Ltd, of the group companies or of third party owners who have authorized their inclusion on
                      the website. Therefore, any use and / or reproduction thereof without the express consent of Sudeep Nutrition Pvt. Ltd. through any means,
                      including the Internet (domain names, social networks, blogs, etc.) is prohibited.</p>
                      <p class="mech-text">Sudeep Nutrition Pvt. Ltd will not be responsible for the infringement of the intellectual or industrial property rights of third parties that may arise
                      from the inclusion on the website of brands, trade names, industrial designs, patents, designs, texts, photographs, images, illustrations, graphics,
                      logos, icons or software belonging to third parties that have declared to be the owners of the same when including them on the website.</p>
                      <p class="mech-text">In no case will it be understood that the access and navigation of the User implies an authorization or that there is a waiver, transmission, total or
                      partial transfer of said rights or the granting of any right or expectation of right and in particular, of the alteration, transformation, exploitation,
                      reproduction, distribution or public communication of said content without the express prior authorization of Sudeep Nutrition Pct, Ltd, of any of
                      the group companies or of the corresponding owners.</p>

                      <h5 class="h4" style="margin-top:3rem;"><strong>4.- Responsibilities</strong> </h5>
                      <p class="mech-text">The User acknowledges and accepts that the use of the Web page and its services is carried out under his entire responsibility.</p>
                      <p class="mech-text">The links or mentions contained in the Website have an exclusively informative purpose and, in no case, do they imply the support, approval,
                      commercialization or any relationship between Sudeep Nutrition Pvt. Ltd the people or entities author and / or managers of such content or owners
                      of the sites where be found.</p>

                      <h5 class="h4" style="margin-top:3rem;"><strong>5.- Obligations of the User</strong> </h5>
                      <p class="mech-text">The User undertakes to use the contents of the Web page in a diligent, correct and lawful manner.</p>
                      <p class="mech-text">You can consult our privacy and cookies policy in the specific sections dedicated to this purpose.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 col-section">
            
            </div>
        </div>
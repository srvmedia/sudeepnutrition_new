<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Template 
    {
        var $ci;
         
        function __construct() 
        {
            $this->ci =& get_instance();
        }
		function load($template = '', $view = '' , $view_data = array(), $return = FALSE)
			{               
				//$this->CI =& get_instance();
				//$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
				//return $this->CI->load->view($template, $this->template_data, $return);
				$data['list'] = $view_data;
				$this->ci->load->model('commonmodel');
				$data['list']['adminuser'] = $this->ci->commonmodel->select(NULL,'tbl_admin');
				$username=$this->ci->session->userdata('username');
				//$conditionvalo = array(
                //   'volunteer_username'  => $username
               //);
				//$data['list']['volunteeruser'] = $this->ci->commonmodel->select($conditionvalo,'tbl_volunteer');
				
				$this->ci->load->view('sidebar',$data, $return);
				$this->ci->load->view($view,$data, $return);
				$this->ci->load->view('footer',$data,$return);
			}
			function loadfront($template = '', $view = '' , $view_data = array(), $return = FALSE)
			{               
				//$this->CI =& get_instance();
				//$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
				//return $this->CI->load->view($template, $this->template_data, $return);
				$data['list'] = $view_data;
				$this->ci->load->model('frontmodel');
				//$data['list']['aboutshort'] = $this->ci->frontmodel->select(NULL,'tbl_about');
				$data['list']['contactadress'] = $this->ci->frontmodel->select(NULL,'tbl_contactaddress');
				
				$data['list']['menus'] = $this->ci->frontmodel->select_menu(NULL,'tbl_menu');
				
				$pagecondition=array('page_showinmenu'=>1);
				$data['list']['pages'] = $this->ci->frontmodel->select_dec_asc($pagecondition,'tbl_page',NULL,'sort_order');
				
				$data['list']['placements']=$this->ci->frontmodel->select(NULL,'tbl_placements');
				$dept_condition=array('show_in_event'=>1);
				$data['list']['department']=$this->ci->frontmodel->select_dec_asc($dept_condition,'tbl_department',NULL,'sort_order');

				///////////////
				// $this->ci->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
				// $this->ci->output->set_header("Cache-Control: post-check=0, pre-check=0");
				// $this->ci->output->set_header("Pragma: no-cache");
				////////////////////
				$this->ci->load->view('include/header-old',$data, $return);
				$this->ci->load->view($view,$data, $return);
				$this->ci->load->view('include/footer-old',$data,$return);
			}
    }
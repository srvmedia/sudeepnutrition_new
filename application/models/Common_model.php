<?php

class Common_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    
	
	function get_all_states()
    {
		$this->db->select('*');
		$this->db->from('states');
		$query = $this->db->get();
		$states = $query->result();
		return $states;
	}
	
	function get_all_cities()
    {
		$this->db->select('*');
		$this->db->from('cities');
		$query = $this->db->get();
		$cities = $query->result();
		return $cities;
	}
	
	function get_all_job_types()
    {
		$this->db->select('*');
		$this->db->from('job_types');
		$query = $this->db->get();
		$job_types = $query->result();
		return $job_types;
	}

}

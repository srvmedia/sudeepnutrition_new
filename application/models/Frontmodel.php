<?php
class frontmodel extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_last_ten_entries()
    {
        $query = $this->db->get('tbl_jobs', 10);
		echo $this->db->last_query();exit;
        return $query->result();
    }
	
	function select($condition=NULL,$table,$orderby=NULL)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		if($orderby!=NULL)
		{
			$this->db->order_by($orderby,'asc');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();exit;
        return $query->result_array();
    }
	function select_dec_asc($condition=NULL,$table,$desc=NULL,$asc=NULL)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		if($desc!=NULL)
		{
			$this->db->order_by($desc,'desc');
		}
		if($asc!=NULL)
		{
			$this->db->order_by($asc,'asc');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();exit;
        return $query->result_array();
    }
	function select_orderdesc($condition=NULL,$table,$orderby=NULL)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		if($orderby!=NULL)
		{
			$this->db->order_by($orderby,'desc');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();exit;
        return $query->result_array();
    }
	function select_menu($condition=NULL,$table)
    {
        $this->db->select('*');
		$this->db->join('tbl_page','tbl_page.page_id=tbl_menu.menu_page_id');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		$query =$this->db->order_by('tbl_menu.menu_priority','asc'); 
		$query =$this->db->get($table); 
        return $query->result_array();
    }
	
	
	function select_where($table,$title,$location)
    {
        $this->db->select('*');
		$this->db->join('tbl_employer','tbl_employer.employer_id=tbl_job.job_postedby','left');
		$this->db->like('job_location',$location, 'both'); 
		$this->db->or_like('job_title',$title, 'both');
	
		$query =$this->db->get($table); 
		//echo $this->db->last_query();exit;
        return $query->result_array();
    }
	function select_limit($condition=NULL,$table,$page,$limit,$asc=NULL)
    {
		
        $this->db->select('*');
		$this->db->order_by("events_date", "desc"); 
		if($asc!=NULL)
		{
			$this->db->order_by($asc,'asc');
		}
		
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		$this->db->limit($limit,$page);
		$query =$this->db->get($table); 
		//echo $this->db->last_query();exit;
        return $query->result_array();
    }
	function select_join($condition=NULL,$table,$join)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		foreach($join as $key=>$val)
		{
			$this->db->join($key,$val,'inner');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();
        return $query->result_array();
    }
	function select_join_order_desc($condition=NULL,$table,$join,$orderby)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		foreach($join as $key=>$val)
		{
			$this->db->join($key,$val,'inner');
		}
		if($orderby!=NULL)
		{
			$this->db->order_by($orderby,'desc');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();
        return $query->result_array();
    }
	function select_joinlimit($condition=NULL,$table,$join=NULL,$page=NULL,$limit=NULL,$order=NULL)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		foreach($join as $key=>$val)
		{
			$this->db->join($key,$val,'inner');
		}
		if($order!=NULL)
		{
		$this->db->order_by($order,'desc');
		}
		if($page==NULL)
		{
		$query =$this->db->get($table, $limit); 
		}
		else
		{
			$this->db->limit($limit,$page);
			$query =$this->db->get($table); 
		}
		//echo $this->db->last_query();
        return $query->result_array();
    }
    function insert_entry($data,$table)
    {
         $this->db->insert($table,$data);
		 $insert_id = $this->db->insert_id();
   		return  $insert_id;
    }
	function delete_entry($table,$condition)
    {
        $this->db->where($condition);
		$this->db->delete($table); 

    }
    function update_entry($table,$condition,$val)
    {
        $this->db->update($table, $val, $condition);
    }

	//dynamic website
	function get_main_header($id){
		$this->db->select('*');
        $this->db->from('website_sub_tabs');
        $this->db->where("id",$id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

	}

	function get_market_content($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('market_contents');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

	}

	function get_challenges_market($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('challenges_market');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

	}

	function get_product_list_market($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('product_list_market');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

	}
	function get_product_main_content($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('product_main_content');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}

	function get_product_application($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('product_applications');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}

	function get_product_advantages($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('product_Advantages');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}

	function get_product_sub_section($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('product_sub_section');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}

	function get_product_sub_section_icon($tab_id,$sub_tab_id){
		$this->db->select('*');
        $this->db->from('product_sub_section_icon');
        $this->db->where("tab_id",$tab_id);
		$this->db->where("sub_tab_id",$sub_tab_id);
       	$query=$this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
}

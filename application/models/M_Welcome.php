<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Auth
 *
 * @author Anuj (http://anujshah.in)
 */

class M_Welcome extends CI_Model 
{
	function __construct() {
	parent::__construct();
}
	
	public function insertenquiry($data)
	{

		if($this->db->insert('enquiry', $data))
		{
			return true;
		}else{
			return false;
		}	
	}
	
	public function getbannerdata($program_id)
	{
		$this->db->select('*');	
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}	
		
		$query=$this->db->get('banner');

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function getprogramhighlightsdata($program_id)
	{
		$this->db->select('*');
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}
		
		$query=$this->db->get('program_highlights');

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getprogramstructuredata($program_id)
	{
		$this->db->select('*');
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}		
		
		$query=$this->db->get('program_structure');

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getprogramspecializationdata($program_id)
	{
		$this->db->select('*');
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}
		
		$query=$this->db->get('specialization_subject');

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_career_option($program_id)
	{
		$this->db->select('*');
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}
		
		$query=$this->db->get('career_option');

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_eligibility($program_id)
	{
		$this->db->select('*');
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}
		
		$query=$this->db->get('eligibility');

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_program_data($program_id)
	{
		$this->db->select('*');
		if ($program_id) {
			$this->db->where('program_id', $program_id);
		}
		
		$query=$this->db->get('programmes');

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}


}
?>